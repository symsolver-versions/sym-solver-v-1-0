"""
File Purpose: manipulating instantiation of objects

E.g. "if an equivalent object already exists, return it instead of making a new one"

[TODO] use weakref?
"""

from ..finds import find
from ...errors import InputError, InputMissingError


class StoredInstances():
    '''tracks which instances have been stored.
    use list(self) to see list of stored instances.

    force_type: None, or type
        if provided, ensure that all values are this type before storing, via self.store().
        Also if provided, make repr slightly prettier.

    Example setup:

    SYMBOLS = StoredInstances(Symbol)
    def symbol(*args, **kw):
        # create new symbol but first ensure no duplicates; if duplicate return it instead of new symbol.
        creator = Symbol  # Symbol(*args, **kw) will be used to create a new Symbol
        return SYMBOLS.get_new_or_existing_instance(creator, *args, **kw)
    '''
    # # # CREATION # # #
    def __init__(self, force_type=None):
        self.storage = {}
        self.force_type = force_type

    # # # UPDATE CONTENTS # # #
    def store(self, value):
        if self.force_type is not None:
            if not isinstance(value, self.force_type):
                raise TypeError(f'got object of type {type(value).__name__}, expected type {self.force_type.__name__}')
        self.storage[id(value)] = value

    def clear(self, force=False):
        '''clears the contents of self.
        force: bool, default False
            True --> clear contents immediately
            False --> ask for user confirmation first.
        '''
        if not force:
            prompt = f'Are you sure you want to clear this {type(self).__name__}' + \
                     ('' if self.force_type is None else f'containing objects of type {self.force_type.__name__}') + \
                     '?\nIt may lead to unexpected behavior, if the objects previously stored here are ever used again.\n' + \
                     "Please input 'y' or 'yes' to continue, or 'n' or 'no' to abort: "
            choice_orig = input(prompt)
            choice = choice_orig.lower()  # << lowercase
            if choice in ('', 'n', 'no'):
                print('self.clear() aborted by user. Did not clear anything.')
                return
            elif choice not in ('y', 'yes'):
                raise InputError(f"Invalid choice: {repr(choice_orig)}. Expected 'yes' or 'no'.")
            # else, choice is 'y', or 'yes', so continue to clearing...
        self.storage = {}

    # # # GET NEW OR EXISTING INSTANCE # # #
    def get_new_or_existing_instance(self, creator, *args__create, attr_eq_check='__eq__', **kw__create):
        '''if specified object to be created already exists, return it. Otherwise return new one.
        creator: callable
            new_one = creator(*args__create, **kw__create).
            will use new_one to check against objects in self, for equivalency.
        attr_eq_check: string, default '__eq__'
            use (new_one.(attr_eq_check))(obj) when testing for equivalency between new_one and objects in self.
        *args__create and **kw__create are passed to creator.

        sets self._prev_new_or_existing_was_new = True if created a new instance else False.
        '''
        new_one = creator(*args__create, **kw__create)
        obj_equals_new = getattr(new_one, attr_eq_check)
        for existing_obj in self.values():
            if obj_equals_new(existing_obj):
                self._prev_new_or_existing_was_new = False
                return existing_obj
        else:
            self.store(new_one)
            self._prev_new_or_existing_was_new = True
            return new_one

    def get_new_instance(self, creator, *args__create, attr_eq_check='__eq__', kw_increment='id_', **kw):
        '''if specified object to be created already exists, increment kw_increment (from kw) and try again.

        kw_increment: str
            look for this kwarg in kw (it must be provided).
            result = self.get_new_or_existing_instance(creator, *args, **kw)
            if result didn't already exist, return result.
            Otherwise, increment kw_increment by adding 1, then try again.
            To prevent infinite looping,
                if incrementing kw_increment doesn't affect the result
                (checked via attr_eq_check), raise InputError.

        The remaining inputs are the same as those in self.get_new_or_existing_instance():
            creator: callable
                new_one = creator(*args__create, **kw__create).
                will use new_one to check against objects in self, for equivalency.
            attr_eq_check: string, default '__eq__'
                use (new_one.(attr_eq_check))(obj) when testing for equivalency between new_one and objects in self.
            *args__create and **kw__create are passed to creator.
        '''
        try:
            i = kw.pop(kw_increment)
        except KeyError:
            raise InputMissingError(f'Must provide {repr(kw_increment)} in kwargs, or pick a different kw_increment.') from None
        inc = {kw_increment : i}
        kw_call = dict(attr_eq_check=attr_eq_check, **kw)
        result0 = self.get_new_or_existing_instance(creator, *args__create, **inc, **kw_call)
        if self._prev_new_or_existing_was_new:
            return result0
        # else, we need to increment and then try again.
        result_prev = result0
        while True:
            iold = inc[kw_increment]
            inew = iold + 1
            inc[kw_increment] = inew
            result = self.get_new_or_existing_instance(creator, *args__create, **inc, **kw_call)
            if self._prev_new_or_existing_was_new:
                return result
            result__eq__ = getattr(result, attr_eq_check)
            if result__eq__(result_prev):
                errmsg = (f'Incrementing {repr(kw_increment)} from {iold} to {inew} and creating an object '
                          f'led to creating an equivalent object (when compared via obj.{attr_eq_check}). '
                          f'Might be impossible to get a new instance using this scheme. '
                          f'Maybe you meant to provide a different value for kw_increment (got {repr(kw_increment)})')
                raise InputError(errmsg)
            result_prev = result

    # # # REPR # # #
    def __repr__(self):
        _type_str = 'objects' if self.force_type is None else f'{self.force_type.__name__} instances'
        return f'{type(self).__name__} containing {_type_str}: {list(self)}'

    # # # ITERATION / DICT / LIST-like BEHAVIOR # # #
    def keys(self):
        return self.storage.keys()
    def values(self):
        return self.storage.values()
    def items(self):
        return self.storage.items()
    def __iter__(self):
        return iter(self.values())
    def __getitem__(self, key):
        return self.storage[key]
    def __len__(self):
        return len(self.storage)
    def get(self, key, default=None):
        return self.storage.get(key, default=default)

    def index(self, value):
        '''returns key in self equal to value. raise ValueError if none found.'''
        idv = id(value)
        if idv in self.keys():
            return idv
        for key, v in self.items():
            if value==v:
                return key
        raise ValueError(f'{value} not in list')

    def __contains__(self, value):
        try:
            self.index(value)
        except ValueError:
            return False
        else:
            return True

