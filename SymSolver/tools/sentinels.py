"""
File Purpose: Sentinel values (like None, but not built-in to Python)
"""

def _new_not_allowed(cls, *args, **kw):
    '''raises TypeError to indicate new instances of this class are not allowed.'''
    raise TypeError(f'new instances of class {cls} are not allowed.')

class _NO_VALUE():
    ''' Use NO_VALUE to indicate the absense of a value.'''
    def __repr__(self):
        return 'NO_VALUE'

NO_VALUE = _NO_VALUE()
_NO_VALUE.__new__ = _new_not_allowed


class _RESULT_MISSING():
    ''' Use RESULT_MISSING to indicate that the result here is missing,
    e.g. if expected result is an array, some values might be RESULT_MISSING.
    '''
    def __repr__(self):
        return 'RESULT_MISSING'

RESULT_MISSING = _RESULT_MISSING()
_RESULT_MISSING.__new__ = _new_not_allowed