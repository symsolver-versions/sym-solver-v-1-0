"""
File Purpose: Set without hashing

(Set without hashing is slower than set, but behaves like set.)
"""

from .equality import (
    equals,
)


class Set():
    '''"set" which is inefficient (doesn't use hashing) but otherwise behaves like a set.
    Note: Set(Set(contents)) returns a new set with the same exact list of contents.
    E.g. x = Set(mylist); y = Set(x); y.contents is mylist  # << gives True.
    '''
    def __init__(self, contents, equals=equals):
        self.contents = contents
        self.equals = equals
        self._reduce()

    def _reduce(self):
        '''remove duplicates in self.
        Or, flattens self (if self.contents is a Set, self.contents = self.contents.contents)
        '''
        if isinstance(self.contents, Set):
            self.contents = self.contents.contents
        else:
            result = []
            for s in self.contents:
                for r in result:
                    if self.equals(r, s):
                        break
                else:  # didn't break
                    result.append(s)
            self.contents = result

    def __eq__(self, x):
        '''returns self == x. Equality holds when all of the following are True:
        - for s in self, s in x.
        - for s in x, s in self.
        - len(x) == len(self)
        '''
        if x is self:
            return True
        # check lengths first for efficiency in easy False case.
        if len(self) != len(x):
            return False
        # loop through terms; confirm each appears in x else return False
        xl = [t for t in x]
        for term in self:
            for i, xi in enumerate(xl):
                if self.equals(term, xi):
                    xl.pop(i)
                    break
            else:  # didn't break
                return False
        return True

    def __repr__(self):
        return 'Set({})'.format(self.contents)

    def __iter__(self):
        return iter(self.contents)

    def __getitem__(self, i):
        return self.contents[i]

    def __len__(self):
        return len(self.contents)

    def __add__(self, b):
        return Set(list(self) + list(b))

    def __radd__(self, b):
        return Set(list(b) + list(self))

    def __sub__(self, b):
        return Set([s for s in self if not s in b])