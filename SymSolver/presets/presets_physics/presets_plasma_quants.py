"""
File Purpose: "plasma quantities" preset equations

These are:
    - inherently related to physics; specifically, plasma quantities
    - not required for full functionality in other parts of SymSolver
"""

from ..presets_tools import set_presets, load_presets
from ...basics import EquationSystem


''' --------------------- load plasma quants vars --------------------- '''

pq_vars = load_presets('PLASMA_QUANTS_VARS', dst=locals())
# ^ also loads into this namespace: Omegas, omegacs, omegaps, ldebyes, 
#   & versions with i, e, or nothing in place of s.

_fluid_vars = load_presets('FLUID_VARS', dst=locals())
# ^ also loads into this namespace: n, ns, rho, rhos, P, Ps, T, Ts, u, us, v, vs, q, qs, m, ms, g

_em_vars = load_presets('EM_VARS')
E, B, J = (_em_vars[_key] for _key in ('E', 'B', 'J'))

_consts = load_presets('PHYSICAL_CONSTANTS')
eps0, mu0, c = (_consts[_key] for _key in ('eps0', 'mu0', 'c'))


''' --------------------- plasma quants equations --------------------- '''

plasma_quant_1f_eqs_dict = {
    'def_Omegas': (Omegas, qs * B.mag / ms),  # cyclotron
    'def_omegacs': (omegacs, qs * B.mag / ms),  # also cyclotron
    'def_omegaps': (omegaps**2, ns * qs**2 / (ms * eps0)),  # plasma oscillation (aka langmuir)
    'def_ldebyes': (ldebyes**2, eps0 * Ts / (ns * qs**2)),  # debye length (T in energy units)
}

plasma_quant_1f_eqs_s = EquationSystem.from_dict(plasma_quant_1f_eqs_dict)
plasma_quant_1f_eqs_e = plasma_quant_1f_eqs_s.ss('s', 'e').relabeled_via(lambda label: label[:-1] + 'e')
plasma_quant_1f_eqs_i = plasma_quant_1f_eqs_s.ss('s', 'i').relabeled_via(lambda label: label[:-1] + 'i')
plasma_quant_1f_eqs   = plasma_quant_1f_eqs_s.del_ss('s').relabeled_via(lambda label: label[:-1])

eq_def_Omegas, eq_def_omegacs, eq_def_omegaps, eq_def_ldebyes = (plasma_quant_1f_eqs_s[_key]
        for _key in ['def_Omegas', 'def_omegacs', 'def_omegaps', 'def_ldebyes'])
eq_def_Omegae, eq_def_omegace, eq_def_omegape, eq_def_ldebyee = (plasma_quant_1f_eqs_e[_key]
        for _key in ['def_Omegae', 'def_omegace', 'def_omegape', 'def_ldebyee'])
eq_def_Omegai, eq_def_omegaci, eq_def_omegapi, eq_def_ldebyei = (plasma_quant_1f_eqs_i[_key]
        for _key in ['def_Omegai', 'def_omegaci', 'def_omegapi', 'def_ldebyei'])
eq_def_Omega, eq_def_omegac, eq_def_omegap, eq_def_ldebye = (plasma_quant_1f_eqs[_key]
        for _key in ['def_Omega', 'def_omegac', 'def_omegap', 'def_ldebye'])


''' --------------------- Set Presets --------------------- '''

PRESETS_PLASMA_QUANTS = set_presets([*pq_vars.keys(), 'E', 'B', 'eps0', 'c',
        'plasma_quant_1f_eqs_s', 'eq_def_Omegas', 'eq_def_omegacs', 'eq_def_omegaps', 'eq_def_ldebyes',
        'plasma_quant_1f_eqs_e', 'eq_def_Omegae', 'eq_def_omegace', 'eq_def_omegape', 'eq_def_ldebyee',
        'plasma_quant_1f_eqs_i', 'eq_def_Omegai', 'eq_def_omegaci', 'eq_def_omegapi', 'eq_def_ldebyei',
        'plasma_quant_1f_eqs'  , 'eq_def_Omega' , 'eq_def_omegac' , 'eq_def_omegap' , 'eq_def_ldebye' ,
        ],
        locals(), _kind='PLASMA_QUANTS')
