"""
File Purpose: ImportFailed

put this error when an import fails.
This is a separate file from imports_reload.py to mitigate cyclic imports.
"""

import warnings

from ..errors import ImportFailedError
from ..defaults import DEFAULTS


class ImportFailed():
    '''set modules which fail to import to be instances of this class;
    initialize with modulename, additional_error_message.
    when attempting to access any attribute of the ImportFailed object,
    raises ImportFailedError('. '.join(modulename, additional_error_message)).
    Also, if DEFAULTS.IMPORT_FAILURE_WARNINGS, make warning immediately when initialized.

    Example:
    try:
        import zarr
    except ImportError:
        zarr = ImportFailed('zarr', 'This module is required for compressing data.')

    zarr.load(...)   # << attempt to use zarr
    # if zarr was imported successfully, it will work fine.
    # if zarr failed to import, this error will be raised:
    >>> ImportFailedError: zarr. This module is required for compressing data.
    '''
    def __init__(self, modulename, additional_error_message=''):
        self.modulename = modulename
        self.additional_error_message = additional_error_message
        if DEFAULTS.IMPORT_FAILURE_WARNINGS:
            warnings.warn(f'Failed to import module {modulename}.{additional_error_message}')

    def __getattr__(self, attr):
        str_add = str(self.additional_error_message)
        if len(str_add) > 0:
            str_add = '. ' + str_add
        raise ImportFailedError(self.modulename + str_add)