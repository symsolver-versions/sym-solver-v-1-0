"""
Package Purpose: Miscellaneous quality-of-life routines for Multiprocessing tasks.

This package is intended to provide functions which:
- are helpful for solving specific, small problems related to Multiprocessing.
- could be useful in other projects as well

This file:
Imports the main important objects throughout this subpackage.
"""

from .mp_array import (
    MPArray,
)
from .mp_misc import (
    _mpdebug_sleeper,
    _AttrCaller,
)
from . import mp_misc  # save a reference to mp_misc to easily access newly defined funcs there.

