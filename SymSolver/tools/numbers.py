"""
File Purpose: tools related to numerical values / value comparisons
"""

import numbers

from ..defaults import ZERO


''' --------------------- Classify --------------------- '''

def is_integer(x):
    '''return whether x is an integer.
    by first checking isinstance(x, int),
    if that's False, check x.is_integer() if it exists,
    else return False.
    '''
    if isinstance(x, int):
        return True
    else:
        try:
            return x.is_integer()
        except AttributeError:
            return False

def is_number(x):
    '''return whether x is a number.
    returns x.is_number() if possible, else True.
    '''
    try:
        x_is_number = x.is_number
    except AttributeError:
        return True
    else:
        return x_is_number()

def is_real_number(x):
    '''returns whether x is a real number.
    by first checking isinstance(x, complex); if so, return x.imag == 0.
    otherwise, return x.is_real_number() if it exists,
    else return False.
    '''
    if isinstance(x, numbers.Complex):  # note: real numbers are subclasses of numbers.Complex, in python.
        return x.imag == ZERO   # we can use '==' because we know x is a Complex number.
    else:
        try:
            x_is_real_number = x.is_real_number
        except AttributeError:
            return False
        else:
            return x_is_real_number()


''' --------------------- Infinity --------------------- '''

class _Infinity():
    '''mathematical infinity or negative infinity, in the sense of comparisons.

    compares equal to other _Infinity.

    Rather than initialize using this class, use the infinity() function.
    Or, choose your desired already-initialized infinity: POS_INFINITY or NEG_INFINITY
    '''
    def __init__(self, positive=True):
        self.positive = positive
    def __eq__(self, x):
        return isinstance(x, _Infinity) and x.positive==self.positive
    def __gt__(self, x):
        return (self.positive) and (self != x)
    def __ge__(self, x):
        return (self.positive) or (self == x)
    def __lt__(self, x):
        return (not self.positive) and (self != x)
    def __le__(self, x):
        return (not self.positive) or (self == x)
    def __repr__(self):
        return ('+' if self.positive else '-')+'Infinity'
    def __pos__(self):
        return self
    def __neg__(self):
        return infinity(not self.positive)

POS_INFINITY = _Infinity(True)
NEG_INFINITY = _Infinity(False)

POS_INF = PLUS_INF = PLUS_INFINITY = INFINITY = INF = POS_INFINITY
NEG_INF = NEG_INF = MINUS_INF = MINUS_INFINITY = NEG_INFINITY

def infinity(positive=True):
    '''return POS_INFINITY if positive else NEG_INFINITY.'''
    return POS_INFINITY if positive else NEG_INFINITY
