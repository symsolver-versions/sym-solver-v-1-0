"""
File Purpose: "plasma quantities" preset vars

These are:
    - inherently related to physics; specifically, plasma quantities
    - not required for full functionality in other parts of SymSolver
"""

from ..presets_tools import set_presets
from ...initializers import INITIALIZERS
from ...units import UNI


''' --------------------- Frequencies --------------------- '''

Omegas = INITIALIZERS.symbol(r'\Omega', ['s'])  # cyclotron frequency
omegacs = INITIALIZERS.symbol(r'\omega', ['c', 's'])  # also cyclotron frequency
omegaps = INITIALIZERS.symbol(r'\omega', ['p', 's'])  # plasma frequency

for _s in (Omegas, omegacs, omegaps): _s.units_base = UNI.Hz


''' --------------------- Lengths --------------------- '''

ldebyes = INITIALIZERS.symbol(r'\lambda', ['D', 's'])  # Debye length

for _s in (ldebyes,): _s.units_base = UNI.L


''' --------------------- Different subscripts --------------------- '''

Omega = Omegas.del_ss('s')
omegac = omegacs.del_ss('s')
omegap = omegaps.del_ss('s')
ldebye = ldebyes.del_ss('s')

Omegae = Omegas.ss('s', 'e')
omegace = omegacs.ss('s', 'e')
omegape = omegaps.ss('s', 'e')
ldebyee = ldebyes.ss('s', 'e')

Omegai = Omegas.ss('s', 'i')
omegaci = omegacs.ss('s', 'i')
omegapi = omegaps.ss('s', 'i')
ldebyei = ldebyes.ss('s', 'i')


''' --------------------- Set Presets --------------------- '''

PRESETS_PLASMA_QUANTS_VARS = set_presets(('Omega',  'omegac',  'omegap',  'ldebye',
                                          'Omegas', 'omegacs', 'omegaps', 'ldebyes',
                                          'Omegae', 'omegace', 'omegape', 'ldebyee',
                                          'Omegai', 'omegaci', 'omegapi', 'ldebyei',
                                          ), locals(), _kind='PLASMA_QUANTS_VARS')
