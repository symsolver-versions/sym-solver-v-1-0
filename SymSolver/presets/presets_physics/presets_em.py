"""
File Purpose: "E & M" preset vars & equations

These are:
    - inherently related to physics; specifically, the E & M equations
    - not required for full functionality in other parts of SymSolver
"""

from ..presets_tools import set_presets, load_presets
from ...basics import EquationSystem
from ...defaults import ZERO


''' --------------------- load EM vars --------------------- '''

em_vars = load_presets('EM_VARS', dst=locals())
# ^ loads E, B, J into this namespace, too.

_fluid_vars = load_presets('FLUID_VARS')
q, n = (_fluid_vars[_key] for _key in ('q', 'n'))

_consts = load_presets('PHYSICAL_CONSTANTS')
eps0, mu0, c = (_consts[_key] for _key in ('eps0', 'mu0', 'c'))


''' --------------------- EM equations --------------------- '''

em_eqs_dict = {
    'divE': (E.div(), q * n / eps0),   # a.k.a. 'Gauss'
    'divB': (B.div(), ZERO),           # a.k.a. 'Gauss for magnetic fields'
    'curlE': (E.curl(), -B.dpt()),     # a.k.a. 'Induction'
    'curlB': (B.curl(), mu0 * J + c**-2 * E.dpt()),  # a.k.a. 'Ampere with displacement current'
    'curlB_no_disp': (B.curl(), mu0 * J),            # a.k.a. 'Ampere without displacement current'
}

_eqkeys = ('divE', 'divB', 'curlE', 'curlB')
_eqkeys_no_disp = ('divE', 'divB', 'curlE', 'curlB_no_disp')

em_eqs = EquationSystem.from_dict({key: em_eqs_dict[key] for key in _eqkeys})
maxwell_eqs = em_eqs  # alias

em_eqs_no_disp = EquationSystem.from_dict({key: em_eqs_dict[key] for key in _eqkeys_no_disp})
maxwell_eqs_no_disp = em_eqs_no_disp  # alias

eq_divE = em_eqs['divE']
eq_divB = em_eqs['divB']
eq_curlE = em_eqs['curlE']
eq_curlB = em_eqs['curlB']
eq_curlB_no_disp = em_eqs_no_disp['curlB_no_disp']


''' --------------------- Set Presets --------------------- '''

PRESETS_EM = set_presets([*em_vars.keys(), 'eps0', 'mu0', 'c', 'q', 'n',
                            'em_eqs', 'em_eqs_no_disp', 'maxwell_eqs', 'maxwell_eqs_no_disp',
                            'eq_divE', 'eq_divB', 'eq_curlE', 'eq_curlB', 'eq_curlB_no_disp'],
                        locals(), _kind='EM')
