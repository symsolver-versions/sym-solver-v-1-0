"""
File Purpose: Symbol
See also: SYMBOLS, symbol

TODO:
    - test efficiency of using SYMBOLS to help ensure symbol equality
      is determined via object equality, i.e. via 'is', by ensuring that
      when creating a symbol, if any previously-made symbol equals the new one,
      the previous one will be returned instead of the new one.
        - (first adjust __eq__ so that it only checks equality via 'is',
           and use the current __eq__ algorithm to check equality during _symbol_create.)
    - use weakrefs in SYMBOLS -- SYMBOLS should not keep any Symbol from being deleted.
"""
from ..abstracts import (
    AbstractOperation, SubbableObject,
)
from ..initializers import (
    initializer_for,
    INITIALIZERS,
)
from ..tools import (
    _repr,
    equals,
    StoredInstances,
    Binding, format_docstring,
)
from ..defaults import DEFAULTS

binding = Binding(locals())


class Symbol(AbstractOperation, SubbableObject):
    '''treated as a single variable by SymSolver.
    E.g. Symbol('x'), Symbol('n', subscipts=['s'])

    not intended for direct instantiation;
    users should use symbol() or symbols() to create new Symbol object or objects.
    for help on parameters, look to those methods, or to self.__init__.

    Symbol objects are intended to be immutable.
    To make a new Symbol similar to self with some attributes changed, see self._new.

    symbol==x iff x is a Symbol with s and subscripts matching symbol.

    Notes on Immutability:
        s, subscripts, and constant are read-only properties to
        discourage accidentally writing their values.
    '''
    # # # CREATION # # #
    def _init_args(self):
        '''returns args to go before entered args during self._new, for self.initializer.
        self._new(*args, **kw) will lead to self.initializer(*self._init_args(), *args, **kw).
        '''
        return (self.s,)

    def as_constant(self):
        '''returns copy of self with constant=True.'''
        return self._new(constant=True)

    # make s, subscripts, and constant be read-only properties to encourage immutability.
    s = property(lambda self: self._s, doc='''what the Symbol represents''')
    subscripts = property(lambda self: self._subscripts, doc='''subscripts associated with the Symbol''')
    constant = property(lambda self: self._constant, doc='''whether to treate the Symbol like a constant''')

    # # # DISPLAY # # #
    @staticmethod
    def _subscripts_to_str(subscripts):
        '''return string for writing subscipts of Symbol.'''
        subs_str = ', '.join([str(s) for s in subscripts])
        if len(subs_str) > 0:
            subs_str = '_{' + subs_str + '}'
        return subs_str

    def _subs_str(self):
        '''return string for self.subscripts.'''
        return self._subscripts_to_str(self.subscripts)

    def __str__(self):
        return str(self.s) + self._subs_str()

    # # # EQUALITY # # #
    # two Symbols are only equal if they match in these attrs:
    _EQ_TEST_ATTRS = ['s', 'constant', 'subscripts']

    def __eq__(self, b):
        '''return self == b'''
        if b is self:
            return True
        if not isinstance(b, type(self)):
            return False
        for attr in self._EQ_TEST_ATTRS:
            if not equals(getattr(self, attr), getattr(b, attr)):
                return False
        return True

    def _equals0(self):
        '''return self == 0  (i.e., always return False).'''
        return False

    # # # INSPECTION # # #
    def is_constant(self):
        '''returns whether self is a constant.'''
        return self.constant

    def get_symbols(self):
        '''returns symbols in self. I.e.: (self,)'''
        return (self,)

    def get_symbols_in(self, func):
        '''return symbols in self where func holds. I.e.: (self,) if func(self) else ()'''
        return (self,) if func(self) else ()

    # # # SUBSTITUTIONS # # #
    def is_interface_subbable(self):
        '''returns True, because self should appear as an option in a SubstitutionInterface.'''
        return True

_init_paramdocs = \
    '''ARGUMENTS:
    s: object
        instance of Symbol is a "Symbol of s". Will be displayed using str(s).
        Most users will be satisfied to use string s, e.g. s='x' or 'y'.
    subscripts: iterable, default ()
        subscripts associated with self. Order matters.
        input will be converted to tuple to encourage immutability.

    KEYWORD-ONLY ARGUMENTS:
    constant: bool or None, default None
        whether to treat this Symbol like it represents a constant.
        True --> definitely a constant.
        False --> definitely not a constant.
        None --> unsure.'''

with binding.to(Symbol, keep_local=True):
    # define some things outside of the Symbol class, but keep them in the local namespace.
    # this is so that later packages can call these original functions.
    # e.g. vectors will add kwargs to Symbol's __init__, so it needs to reference the __init__ defined here,
    #     and it can't do that from the Symbol class (since it will be overwriting Symbol's __init__).
    #     So instead, vectors can access this __init__ via SymSolver.basics.symbols.__init__.
    @binding
    @format_docstring(paramdocs=_init_paramdocs)
    def __init__(self, s, subscripts=(), *, constant=None):
        '''initialize Symbol self.

        {paramdocs}
        '''
        self._s = s
        self._subscripts = tuple(subscripts)
        self._constant = constant
        super(Symbol, self).__init__()

    @binding
    def _init_properties(self):
        '''returns dict of kwargs to use when initializing another Symbol like self.'''
        kw = super(Symbol, self)._init_properties()
        kw['subscripts'] = self.subscripts
        kw['constant'] = self.constant
        return kw

    @binding
    def _repr_contents(self, **kw):
        '''returns contents to put inside 'Symbol()' in repr for self.'''
        contents = [_repr(self.s, **kw)]
        if len(self.subscripts) > 0:
            contents.append(f'subscripts={_repr(self.subscripts, **kw)}')
        if self.constant:
            contents.append(f'constant={self.constant}')
        return contents
    

''' --------------------- Create a Symbol object --------------------- '''
# SYMBOLS stores all the Symbol objects ever created.
# the idea is that when about to creating a new Symbol which equals one in here,
#   instead return the already-existing Symbol from in here.

SYMBOLS = StoredInstances(Symbol)

@initializer_for(Symbol)
@format_docstring(paramdocs=_init_paramdocs)
def symbol(s, subscripts=(), *, constant=None, **kw):
    '''create a new Symbol using the parameters provided.
    Stores created Symbol in SYMBOLS, and ensure no duplicates:
        if the new Symbol equals any previously-created symbol,
        return the previously-created symbol instead of making a new one.

    {paramdocs}
    '''
    return _symbol_create(s, subscripts=subscripts, constant=constant, **kw)

def _symbol_create(s, *args, **kw):
    '''create new symbol but first ensure no duplicates;
    if duplicate, return previously-created equal symbol.
    Generic args & kwargs --> can be used by subclasses with arbitrary __init__.
    '''
    __tracebackhide__ = DEFAULTS.TRACEBACKHIDE
    return SYMBOLS.get_new_or_existing_instance(Symbol, s, *args, **kw)

def symbols(strings, *args__symbol, **kw__symbol):
    '''create multiple Symbol objects using the provided args and kwargs.

    strings: list of strings, or a single string.
        if a single string, first split into a list of strings via .split()
        create one Symbol for each string here.
        E.g. symbols(('x', 'y', 'z')) <--> symbol('x'), symbol('y'), symbol('z')
        E.g. symbols('a c', constant=True) <--> symbol('a', constant=True), symbol('c', constant=True)

    implementation detail note:
        by using INITIALIZERS.symbol, we ensure that symbols() will remain appropriate
        even if a later module defines a new function as the initializer for Symbol.
    '''
    if isinstance(strings, str):
        strings = strings.split()
    return tuple(INITIALIZERS.symbol(s, *args__symbol, **kw__symbol) for s in strings)
