"""
File Purpose: KeyedSymbolicObject
(directly subclasses SymbolicObject)

Rather than use a list of terms to store info, use a dictionary.
Despite being iterable, intentionally does not inherit from IterableSymbolicObject.
KeyedSymbolicObject is fundamentally different from IterableSymbolicObject in a few ways:
    - comparison can be more efficient for KeyedSymbolicObject,
        as two KeyedSymbolicObject objects must have all the same keys to compare equal.
    - indexing is not supported for KeyedSymbolicObject,
        and it might be confusing to provide indexing methods here.
        The closest thing may be a method for "get the key, given a value".
    - rather than initialize from an expanded list of terms, as in create_object(*terms),
        KeyedSymbolicObject initializes using a dictionary, e.g. create_object(terms_dict).
        This allows to maintain dict properties, e.g. collections.defaultdict

KeyedSymbolicObject is not used anywhere else in abstracts, basics, vectors, or precalc_operators.
An great example of a KeyedSymbolicObject subclass is Polynomial from the polynomials subpackage.
"""

from .iterable_symbolic_objects import IterableSymbolicObject
from .symbolic_objects import (
    SymbolicObject,
    is_constant, is_number,
)
from ..tools import (
    equals, dict_equals,
    _repr,
)


class KeyedSymbolicObject(SymbolicObject):
    '''SymbolicObject containing a dictionary of keys and associated values.

    IMMUTABILITY NOTE:
        KeyedSymbolicObjects should not be altered after their creation;
        instead, create a new KeyedSymbolicObject with the desired alterations.
        There are a few different methods to support this principle and make this task easier,
            such as: [TODO] put method names here.

    KEYS NOTE:
        Parts of the implementation of KeyedSymbolicObject assumes keys are not SymbolicObjects.
        For example, keys are ignored entirely during self.get_symbols(), which only considers values.

    DICT COPYING NOTE:
        [EFF] for efficiency, the input dict is stored internally without first making a copy.
        So, be careful to not edit the input dict outside of this object.
        Note you can copy a dict using dict.copy().
        Also note: using self.copy() will return a new object like self but with the dict copied as well.
    '''
    def __init__(self, dictionary, *args, **kw):
        self.dictionary = dictionary
        SymbolicObject.__init__(self, dictionary, *args, **kw)

    def __eq__(self, b):
        try:
            return super().__eq__(b)
        except NotImplementedError:
            return dict_equals(self.dictionary, b.dictionary)

    # # # DISPLAY # # #
    def _repr_contents(self, **kw):
        '''list of contents to put as comma-separated list into repr of self.'''
        return [_repr(self.dictionary, **kw)]

    # # # STANDARD DICT-LIKE BEHAVIOR # # #
    def __getitem__(self, key):
        '''return self[key]'''
        return self.dictionary[key]

    def keys(self):    return self.dictionary.keys()
    def values(self):  return self.dictionary.values()
    def items(self):   return self.dictionary.items()
    def __len__(self): return len(self.dictionary)
    def get(self, key, default=None):
        return self.dictionary.get(key, default)

    # # # ITERABLE_SYMBOLIC_OBJECT-LIKE BEHAVIOR # # #  
    terms = property(lambda self: self.values(),
        doc='''alias to self.values().
        implementation note: allows self to behave like an IterableSymbolicObject in some ways,
        and to re-use some implementations from IterableSymbolicObject in this class.''')

    def __iter__(self):
        '''returns iter(self.values()). NOTE: this is different behavior than standard dicts.
        implementation note: This allows some methods for IterableSymbolicObject to apply here.
        '''
        return IterableSymbolicObject.__iter__(self)

    def __contains__(self, b):
        '''returns whether b is equal to one of the values of self.'''
        return IterableSymbolicObject.__contains__(self, b)

    contains_deep   = IterableSymbolicObject.contains_deep
    get_symbols     = IterableSymbolicObject.get_symbols
    is_constant     = IterableSymbolicObject.is_constant
    is_number       = IterableSymbolicObject.is_number

    # # # IMMUTABILITY DESIGN INTENTION - SUPPORT # # #
    def copy(self):
        '''returns a NEW OBJECT like self but using a copy of self.dictionary.'''
        return self._new(self.dictionary.copy())

    def updated(self, *update_dict_or_empty, **update_kwargs):
        '''returns a NEW OBJECT like self, after self.dictionary.copy().update(updates)

        update_dict_or_empty: 0 or 1 args.
            If len(update_dict_or_empty) == 0, ignore.
            Else, using E = update_dict_or_empty[0]:
                If E has a .keys() method, then does result[key] = E[key] for key in E.
                Otherwise, does result[key] = value for (key, value) in E.
        update_kwargs:
            for key in update_kwargs, result[key] = update_kwargs[key]
        '''
        if len(update_dict_or_empty) >= 2:
            raise TypeError(f'updated expects 0 or 1 positional args but got {len(update_dict_or_empty)}.')
        dictcopy = self.dictionary.copy()
        dictcopy.update(*update_dict_or_empty, **update_kwargs)
        return self._new(dictcopy)

    def with_key_set_to(self, key, value):
        '''returns a NEW OBJECT like self, but with key set to value.'''
        return self.updated({key: value})
