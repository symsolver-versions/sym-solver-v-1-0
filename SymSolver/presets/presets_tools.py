"""
File Purpose: misc. tools for presets
"""
from ..errors import InputError

PRESETS = dict()  # << all currently-known pre-defined variables / equations / other objects.
PRESET_KINDS = dict()  # << map from "kind" to dict of that kind of presets. (Only for the loaded presets.)

def get_presets(requests, _to_list_if_only_one=False):
    '''returns a list of the requested preset values.
    requests: str or iterable
        str --> iterate through requests.split(' ')
        iterable --> for each request, get PRESETS[request]
    _to_list_if_only_one: bool, default False
        when there is only one request, this tells whether to return list or single result.
        e.g. get_presets('n', False) --> Symbol('n');  get_presets('n', True) --> [Symbol('n')]
    '''
    if isinstance(requests, str):
        requests = requests.split(' ')
    result = []
    for request in requests:
        try:  # << put in a try..except in order to raise a more verbose error if key is invalid.
            preset = PRESETS[request]
        except KeyError:
            errmsg = f"Preset not defined: {repr(request)}. See SymSolver.PRESETS for all available presets."
            raise NotImplementedError(errmsg) from None
        result.append(preset)
    if not _to_list_if_only_one and len(result) == 1:
        return result[0]
    return result

def set_presets(keys, source, *, _kind=None):
    '''sets keys in PRESETS to values from source.
    keys: str or iterable
        str --> iterate through keys.split(' ')
        iterable --> for each key, get source[key]
    source: dict
        mapping from keys to values
    _kind: None or str
        if provided, _register_presets_kind(_kind, result)
        (note: first will convert _kind to UPPERCASE.)

    returns a dict with just the keys listed here.

    example:
        X, Y, Z = symbols(('x', 'y', 'z'))
        i = ImaginaryUnit()
        presets_iXYZ = set_presets(('i', 'X', 'Y', 'Z'), locals())
    '''
    if isinstance(keys, str):
        keys = keys.split(' ')
    result = dict()
    for key in keys:
        val = source[key]
        PRESETS[key] = val
        result[key] = val
    if _kind is not None:
        _register_presets_kind(_kind, result)
    return result

def _register_presets_kind(name, dict_):
    '''registers name as a loaded kind of preset.'''
    PRESET_KINDS[name] = dict_

def load_presets(kind, dst=None):
    '''"loads" presets of this kind.
    "Load" means put into PRESETS; can access via get_presets.
        Also will put into dictionary dst, if provided.
        For example, to make variables accessible directly, use dst=locals().

    returns dict of all presets of this kind.

    kind is case-insensitive (will be internally converted to UPPERCASE).
    "kind" options:
        'all' --> all kinds of presets.
        'physics' --> all presets related to physics.
        'constants' --> all presets for any kind of constants.

        'required' --> required for full functionality within SymSolver.
        'misc' --> miscellaneous; not related to physics.
        'units' --> unit bases.
        'em_vars' --> variables for E & M.
        'em' --> variables & equations for E & M.
        'fluid_vars' --> variables for fluids.
        'fluids' --> variables & equations for fluids.
        'plasma_quants_vars' --> variables for plasma quants.
        'plasma_quants' --> variables & equations for plasma quants.
        'physical_constants' --> physical constants.
    '''
    kind_input = kind
    kind = kind.upper()
    if kind in ('ALL', 'PHYSICS', 'CONSTANTS'):  # --> load multiple subkinds
        if kind == 'ALL':
            subkinds = ('required', 'misc', 'units', 'physics')
        elif kind == 'PHYSICS':
            subkinds = ('fluids', 'em', 'physical_constants', 'plasma_quants')
        else:  # kind == 'CONSTANTS'
            subkinds = ('physical_constants',)
        result = dict()
        for subkind in subkinds:
            loaded_subkind = load_presets(subkind)
            result.update(loaded_subkind)
    else:  # --> load a single kind (directly from an already-defined dict)
        # first, import the appropriate module.
        #    importing the module should update PRESETS and PRESET_KINDS, appropriately.
        #    (If it was already imported, then we assume those dicts are already appropriately set.)
        if kind == 'REQUIRED':
            from . import presets_required
        elif kind == 'MISC':
            from . import presets_misc
        elif kind == 'UNITS':
            from . import presets_units
        elif kind == 'EM_VARS':
            from .presets_physics import presets_em_vars
        elif kind == 'EM':
            from .presets_physics import presets_em
        elif kind == 'FLUID_VARS':
            from .presets_physics import presets_fluid_vars
        elif kind == 'FLUIDS':
            from .presets_physics import presets_fluids
        elif kind == 'PLASMA_QUANTS_VARS':
            from .presets_physics import presets_plasma_quants_vars
        elif kind == 'PLASMA_QUANTS':
            from .presets_physics import presets_plasma_quants
        elif kind == 'PHYSICAL_CONSTANTS':
            from .presets_physics import presets_physical_constants
        else:
            raise InputError(f'invalid "kind": {repr(kind_input)}. See help(load_presets) for details.')
        # then, get the appropriate dict.
        result = PRESET_KINDS[kind]
    # now that we put the loaded keys into result, put that into dst if necessary, then return.
    if dst is not None:
        dst.update(result)
    return result
