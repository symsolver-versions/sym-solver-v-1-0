"""
File Purpose: OperationContainer
(directly subclasses IterableSymbolicObject)
"""

from .iterable_symbolic_objects import IterableSymbolicObject
from ..defaults import DEFAULTS
from ..tools import (
    alias,
)

class OperationContainer(IterableSymbolicObject):
    '''contains Symbolic objects, and applies operations to all objects in self.

    E.g. (LHS = RHS) + x --> LHS + x = RHS + x
    '''
    def apply_operation(self, operation, _prevent_new=False):
        '''return self with operation applied to objects contained in self.
        if _prevent_new, and all resulting objects are unchanged (via 'is'), return self.
        '''
        __tracebackhide__ = DEFAULTS.TRACEBACKHIDE
        opped = [operation(t) for t in self]
        if _prevent_new and all(opi is selfi for opi, selfi in zip(opped, self)):
            return self
        else:
            return self._new(*opped)

    op = alias('apply_operation')

    # # # ARITHMETIC # # #
    def __add__(self, b):      return self.op(lambda x: x + b)
    def __radd__(self, b):     return self.op(lambda x: b + x)
    def __mul__(self, b):      return self.op(lambda x: x * b)
    def __rmul__(self, b):     return self.op(lambda x: b * x)
    def __truediv__(self, b):  return self.op(lambda x: x / b)
    def __rtruediv__(self, b): return self.op(lambda x: b / x)
    def __pow__(self, b):      return self.op(lambda x: x ** b)