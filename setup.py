#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 2021

@author: Sevans
"""

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
   long_description = fh.read()

extras_require = {'all': ['numpy', 'matplotlib', 'mpmath', 'IPython'],  # all additional optional packages
                  'dev_math': ['numpy', 'mpmath'],  # for development but without any displaying.
                  'dev_display': ['matplotlib', 'IPython']  # for development with dispalying.
                  }

setup(
      name     = 'SymSolver',
      version  = '0.2',
      author   = 'Samuel Evans',
      author_email = 'sevans7@bu.edu',
      extras_require = extras_require,
      long_description=long_description,
      long_description_content_type='text/markdown',
      url      = 'https://gitlab.com/Sevans7/symsolver',
      packages = find_packages(),
      )