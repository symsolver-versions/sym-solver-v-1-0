"""
File Purpose: MPArray

Multi-processing processable array.
use MPArray.apply to apply a function individually to each element in self.

The main use-case is when you want to solve many similar independent problems,
and those problems are generated with some array manipulations involved.
e.g. polynomial with array coefficients --> array of polynomials --> find roots of each polynomial.
"""
import multiprocessing as mp

from .mp_misc import _AttrCaller
from ..arrays import itarrayte, array_expand_elements
from ..imports_failed import ImportFailed
from ..progress_updates import ProgressUpdatePrinter
from ..sentinels import RESULT_MISSING
from ...errors import InputError
from ...defaults import DEFAULTS

try:
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')


''' --------------------- MPArray --------------------- '''

class MPArray():
    '''store an array; self.apply(f) applies f to each element of array.

    self.apply(f) returns a same-shaped array with results of f(element) for element in array.
    Those results should be calculated independently, and can be done in parallel via multiprocessing.

    If there is a crash during the call to apply, the intermediate result will be saved to self._crashed_result.
    '''
    # # # CREATION / INITIALIZATION # # #
    def __init__(self, arr):
        self.arr = arr

    def _new(self, *args, **kw):
        '''use this whenever creating a new instance like self.
        The implementation here is equivalent to type(self)(*args, **kw),
        but subclasses might override, e.g. to pass some info about self to the new instance.
        '''
        return type(self)(*args, **kw)

    # # # ARRAY-LIKE BEHAVIOR # # #
    shape = property(lambda self: np.shape(self.arr), doc='''np.shape of arr''')
    size  = property(lambda self: np.size(self.arr),  doc='''np.size of arr''')
    ndim  = property(lambda self: np.ndim(self.arr),  doc='''np.ndim of arr''')
    dtype = property(lambda self: self.arr.dtype, doc='''arr.dtype''')

    def __iter__(self):
        '''iterates through self.arr, yielding (value, multi_index)'''
        return itarrayte(self.arr)

    def __getitem__(self, idx):
        '''return self[idx].
        If this gives a result which is not a numpy array, return it.
        Otherwise, return self._new(result).
        '''
        result = self.arr[idx]
        if isinstance(result, np.ndarray):
            return self._new(result)
        else:
            return result

    element_type = property(lambda self: None if self.size==0 else type(next(iter(self))[0]),
                            doc='''type of first element of arr, or None if arr.size == 0.''')

    # # # DISPLAY # # #
    def __repr__(self):
        '''returns repr for self'''
        array_info_str = ', '.join(f'{key}={val}' for key, val in self._array_info().items())
        return f'{type(self).__name__}(array with {array_info_str})'

    def _array_info(self):
        '''return dict of info about arr, to put into repr of self.'''
        result = dict(shape=self.shape, element_type=self.element_type.__name__)
        return result

    # # # APPLY -- HELPERS # # #
    def _empty(self):
        '''returns an array with shape self.shape filled with the sentinel value RESULT_MISSING'''
        return np.full(self.shape, RESULT_MISSING, dtype=object)

    def call_sp(self, f, *args__f, print_freq=None, **kw__f):
        '''call f individually on each element in self.arr, using "single-processer" (i.e. ignore multiprocessing).'''
        results = self._empty()
        self._crashed_result = results  # << store in case of crash.
        size    = self.size
        updater = ProgressUpdatePrinter(print_freq=print_freq)
        for i, (elem, midx) in enumerate(self):  # midx stands for 'multi-index'
            updater.print(f'Applying {f.__name__}(); {i+1} of {size}', print_time=True)
            results[midx] = f(elem, *args__f, **kw__f)
        updater.finalize(process_name=f.__name__)
        del self._crashed_results  # << there was no crash.
        return results

    def call_mp(self, f, *args__f, ncpu=None, print_freq=None, **kw__f):
        '''call f individually on each element in self.arr, using multi-processing.'''
        results = self._empty()
        self._crashed_results = results  # << store in case of crash.
        size    = self.size
        updater = ProgressUpdatePrinter(print_freq=print_freq)
        ncpuse  = self.ncpuse(ncpu)
        with mp.Pool(processes=ncpuse) as pool:
            result_getters = self._empty()
            for i, (elem, midx) in enumerate(self):  # set up the 'result getters'
                updater.print(f'Setting up getters in pool; {i+1} of {size}', print_time=True)
                result_getters[midx] = pool.apply_async(f, args=(elem, *args__f), kwds=kw__f)
            for i, (elem, midx) in enumerate(self):  # get the results
                updater.print(f'Calculating {f.__name__}(); {i+1} of {size}', print_time=True)
                results[midx] = result_getters[midx].get()
        updater.finalize(process_name=f.__name__)
        del self._crashed_results  # << there was no crash.
        return results

    def ncpuse(self, ncpu=None):
        '''returns number of cpus to use for multiprocessing.

        ncpu: None or value
            None --> ncpu = mp.cpu_count() - 1
        
        returns min(ncpu, self.size)
        '''
        if ncpu is None:
            ncpu = mp.cpu_count() - 1
        return min(ncpu, self.size)

    def apply(self, f, *args__f, ncpu=None, print_freq=None, expand=False, **kw__f):
        '''apply f individually to each of the elements in self.arr.

        f: callable or string
            apply this to each element x, in self.
            callable --> evaluate f(x, *args__f, **kw__f)
                    Note: must be compatible with multiprocessing,
                    i.e. must be defined at the top-level of a module and not a lambda function.
            else --> evaluate x.f(*args__f, **kw__f).
                    e.g. f='roots' --> x.roots(*args__f, **kw__f).
        ncpu: None (default) or number
            number of cpus to use.
            None --> mp.cpu_count() - 1
            False, 0, or 1 --> don't use multiprocessing.
            Also note, will never attempt to use more cpus than self.size.
        expand: bool, default False
            whether to expand lists before returning result.
            False --> result will have same shape as self. 
            True --> if f(element) is iterable, result will have shape (*self.shape, L),
                    with L==len(f(element)) (note: all f(element) lengths must be the same).
                    If expanding fails, pre-expansion result will be stored in self._crashed_results

        returns: an array with same shape as self, containing the results.
        '''
        __tracebackhide__ = DEFAULTS.TRACEBACKHIDE
        # setup
        if callable(f):
            callable_f = f
        else:  # f is a string, hopefully.
            callable_f = _AttrCaller(f)
        ncpuse = self.ncpuse(ncpu)
        # calling the appropriate function:
        if ncpuse <= 1:
            result = self.call_sp(callable_f, *args__f, print_freq=print_freq, **kw__f)
        else:
            result = self.call_mp(callable_f, *args__f, print_freq=print_freq, **kw__f, ncpu=ncpu)
        if expand:
            self._crashed_results = result   # << user can recover result in case of crash.
            result = array_expand_elements(result, allow_non_iterable=True)
            del self._crashed_results  # << didn't crash!
        return result
