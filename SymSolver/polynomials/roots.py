"""
File Purpose: methods related to roots of polynomials.

[TODO] need to do a better job of checking "root_is_bad".
    Currently:
        when numer(root) and denom(root) are both non-zero,
            but "close to 0" in floating precision, results are fine.
        but when numer(root) == 0, current algorithm says "definitely a root".
            But we should check whether denom(root) is "close to 0" somehow.
            (maybe by testing numer(root+eps) / denom(root+eps) for "very small" eps?)
        and when denom(root) == 0, not sure what current algorithm will say,
            maybe a divide by 0 error? We should debug this as well.
"""

from ..tools import ImportFailed
try:
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')
try:
    import mpmath as mpm
except ImportError:
    mpm = ImportFailed('mpmath')

from .polynomial import Polynomial
from .polyfraction import PolyFraction
from ..errors import (
    PolynomialPatternError, PolyFractionPatternError,
    PolynomialNotImplementedError,
    InputError,
)
from ..tools import (
    array_select_max_imag, array_select_min_imag,
    array_select_max_real, array_select_min_real,
    Binding, format_docstring, alias,
)
from ..defaults import DEFAULTS

binding = Binding(locals())


''' --------------------- roots tools --------------------- '''

_root_select_paramdocs = '''roots: array
        E.g. array of shape (70,50,3) for roots of 70 x 50 cubic polynomials.
    axis: int, default -1
        axis where the roots are listed.
    keepdims: bool, default False
        whether to keep dimensions of result.
        E.g. for (70,50,3) shape input, result will have shape (70,50,1) if keepdims, else (70,50).'''

@format_docstring(paramdocs=_root_select_paramdocs)
def roots_max_imag(roots, axis=-1, keepdims=False):
    '''selects root with maximum imaginary part.
    Note this is just an alias to tools.array_select_max_imag.

    {paramdocs}   
    '''
    return array_select_max_imag(roots, axis=-1, keepdims=keepdims)

@format_docstring(paramdocs=_root_select_paramdocs)
def roots_min_imag(roots, axis=-1, keepdims=False):
    '''selects root with minimum imaginary part.
    Note this is just an alias to tools.array_select_min_imag.

    {paramdocs}   
    '''
    return array_select_min_imag(roots, axis=-1, keepdims=keepdims)

@format_docstring(paramdocs=_root_select_paramdocs)
def roots_max_real(roots, axis=-1, keepdims=False):
    '''selects root with maximum real part.
    Note this is just an alias to tools.array_select_max_real.

    {paramdocs}   
    '''
    return array_select_max_real(roots, axis=-1, keepdims=keepdims)

@format_docstring(paramdocs=_root_select_paramdocs)
def roots_min_real(roots, axis=-1, keepdims=False):
    '''selects root with minimum real part.
    Note this is just an alias to tools.array_select_min_real.

    {paramdocs}   
    '''
    return array_select_min_real(roots, axis=-1, keepdims=keepdims)


''' --------------------- roots for a single Polynomial or PolyFraction --------------------- '''

with binding.to(Polynomial):
    @binding
    def numpy_roots(self, **kw__None):
        '''returns roots of self, by converting self to a single numpy polynomial and getting its roots.
        Requies all coefficients of self to be non-iterable. (E.g., not arrays. See also: self.to_mp_array().)
        '''
        np_poly = self._to_numpy_single()
        return np_poly.roots()

    @binding
    @format_docstring(default_extraprec=DEFAULTS.POLYROOTS_EXTRAPREC)
    def mpmath_roots(self, *, extraprec=None, as_array=True, **kw__mpmath_polyroots):
        '''returns roots of self, by using mpmath.polyroots.
        Requies all coefficients of self to be non-iterable. (E.g., not arrays. See also: self.to_mp_array().)

        extraprec: None or int
            extra precision to use during root-find algorithm; passed directly to mpmath.polyroots.
            None --> use DEFAULTS.POLYROOTS_EXTRAPREC (default: {default_extraprec})
        as_array: bool, default True
            whether to convert result of mpmath.polyroots to array (of complex numbers), before returning.
            True --> convert result to array of complex numbers.
            False --> return result of mpmath.polyroots, unchanged. CAUTION: issues if applying numpy funcs.
                    (example issue: np.imag(result) will be an array of 0's.)

        extra kwargs are passed directly to mpmath.polyroots.
        '''
        if extraprec is None: extraprec = DEFAULTS.POLYROOTS_EXTRAPREC
        coeff = self.coef_list(reverse=True, iterable_ok=False)
        result = mpm.polyroots(coeff, extraprec=extraprec, **kw__mpmath_polyroots)
        return np.array(result, dtype=complex) if as_array else result

    Polynomial.np_roots = alias('numpy_roots')
    Polynomial.mpm_roots = alias('mpmath_roots')

    @binding
    @format_docstring(default_mode=DEFAULTS.POLYROOTS_MODE, default_easycheck=DEFAULTS.POLYROOTS_EASYCHECK)
    def roots(self, roots_mode=None, *, _easy_check=None, **kw):
        '''return roots of self.
        Requies all coefficients of self to be non-iterable. (E.g., not arrays. See also: self.to_mp_array().)

        roots_mode: None or str
            how to calculate the roots.
            None --> use DEFAULTS.POLYROOTS_MODE  (default: {default_mode})
            'numpy' or 'np' --> use self.numpy_roots(**kw)
            'mpmath' or 'mpm' --> use self.mpmath_roots(**kw)
        _easy_check: None or bool, default None
            whether to check self.easy_roots() first.
            [EFF] Improves efficiency if self has easy roots (e.g. quadratic or linear).
            None --> use DEFAULTS.POLYROOTS_EASYCHECK   (default: {default_easycheck})
        '''
        # easycheck?
        if _easy_check is None: _easy_check = DEFAULTS.POLYROOTS_EASYCHECK
        if _easy_check:
            try:
                return self.easy_roots(**kw)
            except (PolynomialNotImplementedError, PolynomialPatternError):
                pass  # no easy roots found; do more generic root finding.
        # setup
        if roots_mode is None:
            roots_mode = DEFAULTS.POLYROOTS_MODE
        # find roots
        if roots_mode in ('numpy', 'np'):
            result = self.numpy_roots(**kw)
        elif roots_mode in ('mpmath', 'mpm'):
            result = self.mpmath_roots(**kw)
        else:
            raise InputError(f"invalid roots_mode. Expected 'numpy' or 'mpmath' but got {repr(roots_mode)}")
        self._latest_roots_method = roots_mode
        return result

    @binding
    def easy_roots(self, **kw__None):
        '''gets roots using a simple formula. raise error if this is not possible.
        (error type will be PolynomialPatternError OR PolynomialNotImplementedError)
        Simple formulae attempted here include:
            a x + b --> -b / a
            a x^2 + b x + c --> (-b +- sqrt(b^2 - 4 a c)) / (2 a)

        returns: array of roots; result[i] is i'th root
            if self implies an array of polynomials (i.e. self has array coefs),
            then result[..., i] is i'th root, instead.
            This is true even if there is only 1 root.
            E.g. input with coefs of shape (4,5) -->
                result of shape (4,5,2) for quadratic, or (4,5,1) for linear.
        '''
        if len(self) > 3:  # x^0, x^1, x^2  (3 terms)
            raise PolynomialNotImplementedError('No easy roots for polynomial with len > 3')
        coefs = self.coef_list(reverse=True)  # highest-power to lowest-power.
        L = len(coefs)
        if len(coefs) > 3:  # x^0, x^1, x^2  (3 terms)
            raise PolynomialNotImplementedError('No easy roots for polynomial with degree > 2')
        if len(coefs) == 3:
            a, b, c = coefs
            t2 = np.emath.sqrt(b**2 - 4*a*c) / (2*a)  # emath.sqrt(real input < 0) --> complex; sqrt(...) --> nan.
            t1 = -b / (2*a)
            root1 = t1 - t2
            root2 = t1 + t2
            result = np.concatenate([np.expand_dims(root1, axis=-1), np.expand_dims(root2, axis=-1)], axis=-1)
        elif len(coefs) == 2:
            a, b = coefs
            result = np.expand_dims(-b / a, axis=-1)
        else:
            raise PolynomialPatternError('No easy roots for polynomial with degree == 0')
        self._latest_roots_method = 'easy'
        return result

    @binding
    @format_docstring(default_mode=DEFAULTS.POLYROOTS_MODE, default_easycheck=DEFAULTS.POLYROOTS_EASYCHECK)
    def array_roots(self, roots_mode=None, *, expand=True, _easy_check=None, **kw):
        '''return array of roots from self (with array coefficients).
        returns self.easy_roots(**kw) if possible,
        otherwise self.to_mp_array().apply('roots', **kw).

        Also sets self._latest_array_roots_method to 'easy', or roots_mode.

        roots_mode: None or str
            how to calculate the roots, if self.roots_easy() fails.
            None --> use DEFAULTS.POLYROOTS_MODE  (default: {default_mode})
            'numpy' or 'np' --> use self.numpy_roots(**kw)
            'mpmath' or 'mpm' --> use self.mpmath_roots(**kw)
        expand: bool, default True
            whether to expand array elements from apply('roots').
            True --> result will have shape (*self.shape, N roots per polynomial)
            False --> result will have same shape as self, and each element will be a list of roots.
        _easy_check: None or bool, default None
            whether to check self.easy_roots() first.
            [EFF] Improves efficiency if self has easy roots (e.g. quadratic or linear).
            None --> use DEFAULTS.POLYROOTS_EASYCHECK   (default: {default_easycheck})

        Might consume lots of memory / time.
        For direct usage, recommend to call these separately,
        e.g. separately store self.to_mp_array(), in case you want to re-use it.
        [TODO] implement caching for result of self.to_mp_array(), instead?
        '''
        roots = None
        if _easy_check is None: _easy_check = DEFAULTS.POLYROOTS_EASYCHECK
        if _easy_check:
            try:
                roots = self.easy_roots(**kw)
            except PolynomialPatternError:
                pass  # << easy check failed; handle below.
        if roots is None:
            parr = self.to_mp_array()
            roots = parr.apply('roots', roots_mode=roots_mode, expand=expand, **kw)
        self._latest_array_roots_method = self._latest_roots_method
        return roots


with binding.to(PolyFraction):
    @binding
    def roots(self, careful=False, **kw__roots):
        '''returns roots of numerator. Assumes numerator has numerical coefficients, each of which is non-iterable.

        careful: bool, default False
            whether to check that roots are okay, by plugging root into self.
            For example, x=-1 should not be a root of (x**2 + 4x + 3) / (x+1), but it is a root of the numerator.
                (For this exact example there may be a divide by 0 issue.
                But when considering more complicated polynomials, comparison of finite-precision
                floating-point numbers sometimes leads to 0/0 that is not quite 0/0 but does
                represent a case where the root is "fake" and should be discarded.)
            This is more computationally expensive but helps prevent misleading results.
            See also self.roots_careful()
        '''
        if careful:
            return self.roots_careful(**kw__roots)
        else:
            return self.numer.roots(**kw__roots)


''' --------------------- roots sorted --------------------- '''

with binding.to(Polynomial, PolyFraction):
    @binding
    def roots_sorted(self, key=lambda roots: roots, reverse=False, **kw__roots):
        '''return roots of self, sorted via np.argsort(key(roots)).
        if reverse, return result[::-1] instead.
        '''
        roots = self.roots(**kw__roots)
        result = roots[np.argsort(key(roots))]
        if reverse:
            return result[::-1]
        else:
            return result

    @binding
    def roots_sorted_by_imag(self, descending=True, **kw__roots):
        '''returns roots of self, sorted by imaginary part. (default: largest first.)

        descending: bool, default True
            whether to go from largest imaginary part to smallest imaginary part.
            (e.g. if True, the root with largest imaginary part will be first.)
        '''
        return self.roots_sorted(key=np.imag, reverse=descending, **kw__roots)


''' --------------------- careful roots (for PolyFraction) --------------------- '''

with binding.to(PolyFraction):
    @binding
    def _default_tol_if_None(self, tol=None):
        '''returns tolerance to use for determining whether roots are bad.
        tol: None (default) or any value.
            tolerance for "closeness to 0"; self(root) is "close to 0" if |self(root)| < tol.
            None -> use self._tol_root if provided, else DEFAULTS.POLYFRACTION_TOL_ROOT
        '''
        if tol is None:
            tol = self._tol_root
            if tol is None:
                tol = DEFAULTS.POLYFRACTION_TOL_ROOT
        return tol

    @binding
    def roots_are_bad(self, roots, tol=None):
        '''check which roots give |self(root)| > tol.
        returns array of True/False values, of same length as roots, with
            result[i] True  <--> roots[i] is BAD
            result[i] False <--> roots[i] is okay.

        roots: any values
            plug into self. A root is considered "okay" if self(root) is close to 0.
        tol: None (default) or any value.
            tolerance for "closeness to 0"; self(root) is "close to 0" if |self(root)| < tol.
            None -> use self._tol_root if provided, else DEFAULTS.POLYFRACTION_TOL_ROOT
        '''
        tol = self._default_tol_if_None(tol)
        roots = np.asanyarray(roots)
        vals = self(roots)
        return (np.isnan(vals) | (np.abs(vals) > tol))

    @binding
    def select_bad_roots(self, roots, tol=None):
        '''returns array of all values from roots which are BAD roots.
        "bad root" if |self(root)| > tol, i.e. if self(root) is NOT "close to 0".
        '''
        roots = np.asanyarray(roots)
        return roots[self.roots_are_bad(roots, tol=tol)]

    @binding
    def select_good_roots(self, roots, tol=None):
        '''returns array of all values from roots which are good roots.
        "good root" if |self(root)| <= tol, i.e. if self(root) is "close to 0".
        '''
        roots = np.asanyarray(roots)
        return roots[~self.roots_are_bad(roots, tol=tol)]

    @binding
    def roots_careful(self, tol=None, **kw__roots):
        '''returns roots of self, after removing any with self(root) not close to 0.
        "close to 0" means "absolute value is less than tol".

        This function avoids the issue with self.roots(), which only checks roots of numerator,
        so if denominator shares any of those roots then self.roots() may be misleading.
        For example, x=-1 should not be a root of (x**2 + 4x + 3) / (x+1), but it is a root of the numerator.

        [EFF] This is more computationally expensive than self.roots(), due to the self(roots) calculation.
        '''
        return self.select_good_roots(self.roots(careful=False, **kw__roots), tol=tol)

    @binding
    def count_roots(self, careful=False, **kw__roots):
        '''returns len(self.roots()), or len(self.roots_careful()) if careful=True.
        Convenience method, useful with MPArray.apply for debugging / inspecting data.
        '''
        return len(self.roots(careful=careful, **kw__roots))

    @binding
    def count_roots_good_and_bad(self, **kw__roots):
        '''returns (number of good roots, number of bad roots).'''
        roots = self.roots(**kw__roots)
        roots_careful = self.select_good_roots(roots)
        Ngood = len(roots_careful)
        return (Ngood, len(roots) - Ngood)


''' --------------------- [EFF] "first" "good" root for PolyFraction --------------------- '''
# since roots_careful requires evaluating self(root) for ALL roots,
# if you are going to pick only 1 root anyways it may be more efficient to only check 1 at a time.
# (only more efficient if sorting to decide pick order is faster than the evaluating of self(root).)

with binding.to(PolyFraction):
    @binding
    def root_is_bad(self, root, tol=None):
        '''check whether self(root) is greater than tol (in absolute value).
        bool(result) gives:
            False <--> root is "okay",  i.e. self(root) IS close to zero
            True  <--> root is "BAD",   i.e. self(root) is NOT close to zero.

        root: any value.
            plug into self; considered an "okay" root if self(root) is close to 0.
        tol: None (default) or any value.
            tolerance for "closeness to 0"; self(root) is "close to 0" if |self(root)| < tol.
            None -> use self._tol_root if provided, else DEFAULTS.POLYFRACTION_TOL_ROOT

        Returns False if |self(root)| < tol; else returns self(root).
        '''
        tol = self._default_tol_if_None(tol)
        val = self(root)
        if np.isnan(val) or (abs(val) > tol):
            return val     # abs(self(root)) too big. (Sidenote: bool(nan)==True.)
        else:
            return False   # abs(self(root)) < tol, so root is "close to 0".

    @binding
    def first_good_root(self, sortby=lambda roots: roots, *, reverse=False, tol=None, **kw__roots):
        '''returns first root of self which is an okay root.
        "okay root" means self(root) is close to 0. More precisely, it means |self(root)| <= tol.
        "first" is with respect to the sorted list of roots, sorted by sortby.
        
        sortby: function(roots). Default: roots --> roots.
            sort roots according to this order, via np.argsort(sortby(roots)).
            Example: to sort by imaginary part, use
                lambda roots: np.imag(roots)
        reverse: bool, default False
            whether to reverse the result from sorting.
            Example: to sort by imaginary part, in reverse order (i.e. with largest first), use
                sortby = lambda roots: np.imag(roots);  reverse = True

        returns root.
        If NO roots are good, instead returns np.nan (or np.nan + np.nan * 1j if any roots are complex).
        '''
        # get sorted roots
        roots = self.roots(**kw__roots)
        roots_argsort = np.argsort(sortby(roots))
        if reverse:
            roots_argsort = roots_argsort[::-1]
        # test roots
        any_complex = False  # << track whether any roots are complex
        for i in roots_argsort:
            root = roots[i]
            if (not any_complex) and (isinstance(root, complex)):
                any_complex = True
            if not self.root_is_bad(root, tol=tol):
                return root
        # didn't find any good root
        return (np.nan + 1j * np.nan) if any_complex else np.nan


''' --------------------- roots pick max or min --------------------- '''

with binding.to(Polynomial):
    @binding
    def root_max_imag(self, **kw__roots):
        '''returns the root from self with the largest imaginary part.'''
        roots = self.roots(**kw__roots)
        return roots[np.argmax(np.imag(roots))]

    @binding
    def root_min_imag(self, **kw__roots):
        '''returns the root from self with the smallest imaginary part.'''
        roots = self.roots(**kw__roots)
        return roots[np.argmin(np.imag(roots))]

    @binding
    def root_max_real(self, **kw__roots):
        '''returns the root from self with the largest real part.'''
        roots = self.roots(**kw__roots)
        return roots[np.argmax(np.real(roots))]

    @binding
    def root_min_real(self, **kw__roots):
        '''returns the root from self with the smallest real part.'''
        roots = self.roots(**kw__roots)
        return roots[np.argmin(np.real(roots))]


with binding.to(PolyFraction):
    _careful_tol_paramdocs = \
        '''careful: bool, default True
            whether to consider careful roots only.
            True --> check to ensure that the result is an "okay root" of self, i.e. it is close to 0.
                    if it is not an "okay root", try the next root instead (repeating as necessary).
            False --> don't check whether roots are okay; just use self.roots().
        tol: None (default) or any value.
            tolerance for "closeness to 0", if careful=True.
            self(root) is "close to 0" if |self(root)| < tol.
            None -> use self._tol_root if provided, else DEFAULTS.POLYFRACTION_TOL_ROOT.'''

    @binding
    @format_docstring(paramdocs=_careful_tol_paramdocs)
    def _root_pick_from_key(self, sortby=lambda roots: roots, *, reverse=False, careful=True, tol=None, **kw__roots):
        '''returns first root from self...

        sortby: function(roots) --> list
            sort roots by this function, to determine order of roots, so that "first" can be chosen.
            default: roots --> roots, unchanged.
        reverse: bool, default False
            whether to reverse the sorted list after sortby(roots).
        {paramdocs}
        '''
        if careful:
            return self.first_good_root(sortby=sortby, reverse=reverse, tol=tol, **kw__roots)
        else:
            return self.roots_sorted(key=sortby, reverse=reverse, **kw__roots)[0]

    @binding
    @format_docstring(paramdocs=_careful_tol_paramdocs)   
    def root_max_imag(self, careful=True, *, tol=None, **kw__roots):
        '''returns the root from self with the largest imaginary part.

        {paramdocs}
        '''
        return self._root_pick_from_key(sortby=np.imag, reverse=True, careful=careful, tol=tol, **kw__roots)

    PolyFraction.growth_root = alias('root_max_imag')

    @binding
    @format_docstring(paramdocs=_careful_tol_paramdocs)   
    def root_min_imag(self, careful=True, *, tol=None, **kw__roots):
        '''returns the root from self with the largest imaginary part.

        {paramdocs}
        '''
        return self._root_pick_from_key(sortby=np.imag, reverse=False, careful=careful, tol=tol, **kw__roots)

    @binding
    @format_docstring(paramdocs=_careful_tol_paramdocs)   
    def root_max_real(self, careful=True, *, tol=None, **kw__roots):
        '''returns the root from self with the largest imaginary part.

        {paramdocs}
        '''
        return self._root_pick_from_key(sortby=np.real, reverse=True, careful=careful, tol=tol, **kw__roots)

    @binding
    @format_docstring(paramdocs=_careful_tol_paramdocs)   
    def root_min_real(self, careful=True, *, tol=None, **kw__roots):
        '''returns the root from self with the largest imaginary part.

        {paramdocs}
        '''
        return self._root_pick_from_key(sortby=np.real, reverse=False, careful=careful, tol=tol, **kw__roots)
