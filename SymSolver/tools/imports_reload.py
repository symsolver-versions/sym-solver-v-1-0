"""
File Purpose: reloading modules / packages
"""

import importlib
import sys

from .iterables import (
    argsort, sort_by_priorities,
)


''' --------------------- Reloading --------------------- '''

def enable_reload(package='SymSolver'):
    '''smashes the import cache for the provided package.
    All modules starting with this name will be removed from sys.modules.
    This does not actually reload any modules.
    However, it means the next time import is called for those modules, they will be reloaded.
    returns tuple listing of all names of affected modules.
    '''
    l = tuple(key for key in sys.modules.keys() if key.startswith(package))
    for key in l:
        del sys.modules[key]
    return l

def reload(package='SymSolver', prioritize=[], de_prioritize=[]):
    '''reloads all modules from package which have been imported.
    prioritize modules containing any strings in the list prioritize.
    de_prioritize modules containing any strings in the list de_prioritize.
    returns list of names of reloaded modules.
    '''
    reloaded = []
    to_reload = [(name, module) for (name, module) in sys.modules.items() if name.startswith(package)]
    to_reload = sort_by_priorities(to_reload, prioritize, de_prioritize,
                                   equals=lambda priority, name_and_module: priority in name_and_module[0])
    for (name, module) in to_reload:
        importlib.reload(module)
        reloaded += [name]
    return reloaded