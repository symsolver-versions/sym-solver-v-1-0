"""
File Purpose: defines "misc" preset values

These are:
    - not inherently related to physics
    - not required for full functionality in other parts of SymSolver
"""

from .presets_tools import set_presets
from ..basics import symbols
from ..numbers import ImaginaryUnit


''' --------------------- Misc --------------------- '''

i = IUNIT    = ImaginaryUnit()
e,           = symbols(('e',), constant=True)
phi, theta   = symbols((r'\phi', r'\theta'))


''' --------------------- Set Presets --------------------- '''

PRESETS_MISC = set_presets('i e IUNIT phi theta', locals(), _kind='MISC')
