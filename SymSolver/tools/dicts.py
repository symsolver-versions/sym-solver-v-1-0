"""
File Purpose: Dict without hashing, and dictlike behavior for other objects

(Dict without hashing is slower than dict, but behaves like dict.)
"""

from .equality import (
    equals,
)
from .finds import (
    find,
)
from .oop_tools import (
    Binding,
)
from .sentinels import NO_VALUE


def dictlike_in_attr(attr):
    '''returns a wrapper f(cls) which adds methods to the cls so it behaves like a dict.
    Those methods assume the dict-behavior should be associated with attribute attr.
    E.g. attr is the attribute in instances of cls where the dict of information is kept.
    '''
    def cls_but_dictlike_in_attr(cls):
        '''adds methods to cls so it behaves like dict stored in attr, then returns cls.'''
        binding = Binding(locals(), keep_local=True)  # no need to cleanup this namespace after.
        # [low priority][TODO] note: for some reason without keep_local=True, things get messed up,
        #     however recalculating locals() just before exiting 'with' helps somewhat, for some reason....
        with binding.to(cls):
            @binding
            def __setitem__(self, key, value):
                getattr(self, attr)[key] = value
            @binding
            def __getitem__(self, key):
                return getattr(self, attr)[key]
            @binding
            def __delitem__(self, key):
                del getattr(self, attr)[key]
            @binding
            def __contains__(self, key):
                return key in getattr(self, attr)
            @binding
            def keys(self):
                return getattr(self, attr).keys()
            @binding
            def items(self):
                return getattr(self, attr).items()
            @binding
            def values(self):
                return getattr(self, attr).values()
            @binding
            def get(self, key, default=None):
                return getattr(self, attr).get(key, default)
        return cls
    return cls_but_dictlike_in_attr

class Dict():
    '''"dict" which is inefficient (doesn't use hashing) but otherwise behaves like a dict.
    if default is provided (i.e. not NO_VALUE),
        value for key will be assigned default upon first access if get before set.

    [EFF] for efficiency, can use get_i, set_i, del_i methods if index of key is already known.
        This reduces number of key comparisons required (comparing via self.equals).
    '''
    def __init__(self, equals=equals, default=NO_VALUE):
        self._keys = []
        self._values = []
        self.equals = equals
        self.default = default

    def disable_default(self):
        '''sets self.default=NO_VALUE.
        E.g., useful if using default internally but don't want end-result to have a default.
        '''
        self.default = NO_VALUE

    def keys(self):
        return self._keys.copy()
    def values(self):
        return self._values.copy()
    def items(self):
        return list(zip(self._keys, self._values))

    def __repr__(self):
        items_str = ', '.join([f'{key}={value}' for key, value in self.items()])
        return f'Dict({items_str})'

    def __iter__(self):
        return iter(self._keys)

    def _index(self, key):
        '''index of key in self. raise KeyError if not found.'''
        result = self.find(key)
        if result is None:
            raise KeyError(key)
        else:
            return result

    def find(self, key):
        '''find key in self; return index, or None if not found.'''
        return find(self._keys, key, equals=self.equals)

    def __contains__(self, key):
        '''returns key in self'''
        return self.find(key) is not None

    def __getitem__(self, key):
        try:
            i = self._index(key)
        except KeyError:
            if self.default is not NO_VALUE:
                return self.default
            else:
                raise
        return self._values[i]

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def get_i(self, i):
        '''return i'th value in self'''
        return self._values[i]

    def __setitem__(self, key, value):
        i = self.find(key)
        if i is None:
            self._keys.append(key)
            self._values.append(value)
        else:
            self.set_i(i, value, key=key)
            # ^ we set key too because it may be a different object (even though equals(old key, new key)).

    def set_i(self, i, value, *, key=NO_VALUE):
        '''set value for i'th key.
        key: any object, default NO_VALUE
            if provided (i.e. not NO_VALUE), also set i'th key to the value provided.'''
        self._values[i] = value
        if key:
            self._keys[i] = key

    def __len__(self):
        return len(self._keys)

    def __delitem__(self, key):
        i = self._index(key)
        self.del_i(i)

    def del_i(self, i):
        '''delete i'th key & value.'''
        self._keys.pop(i)
        self._values.pop(i)