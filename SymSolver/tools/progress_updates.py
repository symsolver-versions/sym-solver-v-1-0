"""
File Purpose: output something so human sees code is working.
"""

from .timing import TickingWatch
from ..defaults import DEFAULTS


class ProgressUpdatePrinter(TickingWatch):
    '''class for printing messages but only when enough time has passed.
    "Enough" is defined by the input parameter print_freq (in seconds).

    print_freq: None or int (possibly negative or 0)
        Number of seconds between progress updates.
        None --> use DEFAULTS.PROGRESS_UPDATES_PRINT_FREQ
        >0 --> minimum time between calls to self.tick() returning True.
        =0  --> self.tick() will always return True.
        <0 --> self.tick() will always return False.
    wait: bool, default False
        whether to wait until print_freq seconds has passed before doing the first printout.
    print_time: bool, default False
        whether to print time by default
    clearline: bool, default True
        whether to clear the current line before printing.
    clearN: int, default 100
        number of characters to clear if clearing a line.

    Example:
        updater = ProgressUpdatePrinter(print_freq=2, wait=True)
        updater.print('This will not be printed')   # not printed because 2 seconds have not passed yet.
        time.sleep(2.5)  # << wait 2.5 seconds. (Or, put code here which takes >= 2 seconds)
        updater.print('This WILL be printed!')      # prints, then timer is reset,
        updater.print('This will not be printed')   # so it won't print again until 2 more seconds have passed.
    '''
    def __init__(self, print_freq=None, *, wait=False, print_time=True, clearline=True, clearN=100,):
        if print_freq is None:
            print_freq = DEFAULTS.PROGRESS_UPDATES_PRINT_FREQ
        super().__init__(interval=print_freq, wait=wait)
        self.print_time = print_time
        self.clearline  = clearline
        self.clearN     = clearN
    
    print_freq = property(lambda self: getattr(self, 'interval'),
                          lambda self, value: setattr(self, 'interval', value),
                          doc='''alias for self.interval''')

    def print_clear(self, N=None, force=False):
        r'''clears current printed line, and moves cursor to beginning of the line.
        only if self.clearline AND self has printed at least 1 message. OR if force.

        troubleshooting: did you use end='', e.g. print(..., end='')?
            (If not, your print statement may go to the next line.)

        N: None or int
            number of characters to clear.
            None --> use self.clearN instead.
        force: bool, default False
            whether to force print, even if not self.clearline.
            False --> require self.clearline, else don't print.

        Equivalent to:
            print('\r'+ ' '*N +'\r', end='')
        '''
        if force or (self.clearline and self.nticks > 0):
            if N is None: N = self.clearN
            print('\r'+ ' '*N +'\r', end='')

    def force_print(self, *args_message, end='', print_time=None, **kw__print):
        '''prints, without first checking whether to print.
        Clear the line first, if self.clearline.
        print_time: None or bool
            whether to print f'Total time elapsed: {format(t):.2f} seconds.'
            None --> use self.print_time (default: True)
        '''
        self.print_clear()
        if print_time or ((print_time is None) and self.print_time):
            args_message = (*args_message, self.time_elapsed_string())
        print(*args_message, end=end, **kw__print)

    def print(self, *args_message, end='', print_time=None, **kw__print):
        '''prints message given by *args_message, if it is time to print.
        Clear the line first, if self.clearline.
        print_time: None or bool
            whether to print f'Total time elapsed: {format(t):.2f} seconds.'
            None --> use self.print_time (default: True)
        '''
        if self.tick():
            self.force_print(*args_message, end=end, print_time=print_time, **kw__print)

    def printf(self, f=lambda: '', *, end='', print_time=None, **kw__print):
        '''prints message given by calling f() (which must return a single string).
        [EFF] note: only calls f() if actually printing!
            Can use this instead of self.print() if computing the print message might be expensive.

        Clear the line first, if self.clearline.
        print_time: None or bool
            whether to print f'Total time elapsed: {format(t):.2f} seconds.'
            None --> use self.print_time (default: True)

        Example:
            updater = ProgressUpdatePrinter(print_freq=2)
            updater.printf(lambda: '{} '.format(object_whose_string_is_expensive_to_calculate))
        '''
        if self.tick():
            message = f()
            self.force_print(message, end=end, print_time=print_time, **kw__print)

    def finalize(self, process_name=None, *, always=False, **kw__print):
        '''prints 'Completed process in 0.00 seconds!', filling in the process name and time elapsed as appropriate.
        Also, clear the line first, if self.clearline.

        process_name: None (default) or string
            None --> don't print any info about the process which completed.
            string --> include this name in the finalize message.
        always: False (default) or True
            False --> ignore this kwarg.
            True --> always print the finalize message.
        '''
        time_elapsed = self.time()
        if (always) or (self.nticks > 0):
            process_name = '' if process_name is None else repr(process_name) + ' '
            message = f'Completed {process_name}in {time_elapsed:.2f} seconds!'
            self.force_print(message, print_time=False, **kw__print)
