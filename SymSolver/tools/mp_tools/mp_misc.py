"""
File Purpose: Miscellaneous quality-of-life functions for Multiprocessing tasks.
"""
import random
import multiprocessing as mp
import time

from ..sentinels import RESULT_MISSING


''' ----------------------------- Simple Multiprocess ----------------------------- '''

class SimpleMP():
    '''interface to apply a function across a list of (args, kwargs) tuples, using multiprocessing.
    This addresses the simple use-case for multiprocessing:
        given a function f, and a list of [(args1, kw1), (args2, kw2), ..., (argsN, kwN)],
        evaluate and return [f(*args1, **kw1), f(*args2, **kw2), ..., f(*argsN, **kwN)],
        performing each function call in its own process (e.g. in parallel with each other!)

    Working Example:
        multiprocessor = SimpleMP()
        multiprocessor(f, [((7,8), dict(x=9)), ((2,3,4), dict(x=5, y=6)), ((1,), dict())])
            --> returns [f(7, 8, x=9), f(2, 3, 4, x=5, y=6), f(1)]

    Any **kwargs entered during __init__ will be stored and used as defaults when making mp.Pool.

    If the operation crashes (e.g. due to KeyboardInterrupt from a bored user...),
        result can be found in self._crashed_result.
        Not-yet-calculated values there will all be RESULT_MISSING.
    '''
    def __init__(self, **kw__mp_pool_defaults):
        self.kw__mp_pool_defaults = kw__mp_pool_defaults

    def __call__(self, f, arg_kwarg_tuples, **kw__mp_pool):
        '''evaluate and return [f(*args, **kw) for (args, kw) in arg_kwarg_tuples],
        using multiprocessing to perform each function call in its own process (e.g. in parallel).

        f: callable which also appears at the top level of a module
            The object to call on each (*arg, **kw) from arg_kwarg_tuples.
            f must be compatible with multiprocessing, i.e. it must be defined at the top level of a module.
        arg_kwarg_tuples: iterable of tuples
            An iterable of (arg, kwarg) tuples to plug into f.
            For arg_kwarg_tuples = [(args1, kw1), (args2, kw2), ..., (argsN, kwN)],
            this function will return [f(*args1, **kw1), f(*args2, **kw2), ..., f(*argsN, **kwN)].
        '''
        kwpool = self.kw__mp_pool_defaults.copy()
        kwpool.update(kw__mp_pool)

        arg_kwarg_tuples = tuple(arg_kwarg_tuples)  # << if generator, ensure we don't consume it too early

        results = [RESULT_MISSING for _ in arg_kwarg_tuples]
        try:
            with mp.Pool(**kwpool) as pool:
                result_getters = [pool.apply_async(f, args=args, kwds=kw) for (arg, kw) in arg_kwarg_tuples]
                for i, result_getter in enumerate(result_getters):
                    results[i] = result_getter.get()
        except:  # okay to catch all exceptions since we raise immediately.
            self._crashed_result = results
            raise
        else:
            return results


''' ----------------------------- Convenient Pickleable Methods ----------------------------- '''
# Multiprocessing assumes all involved objects (and their contents) are pickleable,
# AND that all every function (and class) involved is defined at the top level of a module.
#   (You can even try to wrap a call to a function with something originally pickleable,
#    but then the result will still not be pickleable since it contains the unpickleable function.)
# A "workaround" is to put any functions you want to use with multiprocessing into a file and import that file.
# This section of this file contains some functions/classes which may be especially useful during multiprocessing.

class _AttrCaller():
    '''_AttrCaller(attr)(obj, *args, **kw) returns getattr(obj, attr)(*args, **kw).
    For convenience, also sets self.__name__ = attr.
    '''
    def __init__(self, attr):
        self.attr = attr
        self.__name__ = attr
    def __call__(self, obj, *args, **kw):
        return getattr(obj, self.attr)(*args, **kw)

def _mpdebug_sleeper(i, mul=1):
    '''prints (i, sleep_s), where sleep_s is a random number between 0 and mul (default mul=1).
    Then sleeps for sleep_s seconds, then returns mul*sleep_s.
    This function helps with debugging multiprocess code.
    '''
    sleep_s = mul*random.random()
    print(f'{i:3d}, {sleep_s:.2f}', flush=True)
    time.sleep(sleep_s)
    return sleep_s

def _mpdebug_add1(val):
    '''just returns val + 1.'''
    return val+1