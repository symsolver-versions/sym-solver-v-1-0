"""
File Purpose: timers, runtime profiling
"""

import cProfile
import pstats
import time


''' --------------------- Runtime Profiling --------------------- '''

class Profile(cProfile.Profile):
    '''runtime profiler. Usage:
    p = Profiler()
    with p:
        >> code to profile goes here <<
    p.print_stats()  # or p.print_stats(key), key='time', 'cumulative', or other key from self.SortKey

    This whole class is a "thin" wrapper around cProfile & pstats
    For a more lightweight form of this class, you can use:
        import cProfile
        p = cProfile.Profile()
        with p:
            >> code to profile goes here <<
        p.print_stats('time')

    sortkey: 'time', 'cumulative', or other key from self.SortKey
        tells default way to sort stats during print_stats.
    clearing: bool, default True
        whether to self.clear() when starting to profile.
        (i.e., when self.enable() is called. Note that self.__enter__() calls self.enable().)
    '''
    def __init__(self, *args, sortkey='time', clearing=True, **kw):
        '''does super().__init__ but also sets self.sortkey.'''
        super().__init__(*args, **kw)
        self.sortkey = sortkey
        self.clearing = clearing

    SortKey = pstats.SortKey  # << for easy reference. See e.g. help(self.SortKey) for details.

    def print_stats(self, sortkey=None):
        '''super().print_stats() using sortkey or self.sortkey if None'''
        if sortkey is None: sortkey = self.sortkey
        super().print_stats(sortkey)

    def enable(self, *args__super, clearing=None, **kw__super):
        '''super().enable(), but first maybe call self.clear().
        clearing: None or bool, default None
            whether to first call self.clear().
            None --> use clearing = self.clearing.
        '''
        if clearing is None: clearing = self.clearing
        if clearing:
            self.clear()
        super().enable(*args__super, **kw__super)

PROFILE = Profile()
def profiling(sortkey=None):
    '''returns default profiler, PROFILE. Sets its sortkey if provided.
    see help(PROFILE.SortKey) for sortkey options.

    Example Usage:
        with profiling():
            >> code to profile goes here <<
        print_profile()
    '''
    if sortkey is not None: PROFILE.sortkey = sortkey
    return PROFILE

def print_profile(sortkey=None):
    '''prints stats from the default profiler (PROFILE).
    see help(PROFILE.SortKey) for sortkey options.

    Example Usage:
        with profiling():
            >> code to profile goes here <<
        print_profile()
    '''
    PROFILE.print_stats(sortkey)

def start_profiling():
    '''equivalent to PROFILE.enable(). Recommended: use profiling() as a context manager instead.
    Example Usage:
        start_profiling()
        >> code to profile goes here <<
        stop_profiling()
    '''
    PROFILE.enable()

def stop_profiling():
    '''equivalent to PROFILE.disable(). Recommended: use profiling() as a context manager instead.
    Example Usage:
        start_profiling()
        >> code to profile goes here <<
        stop_profiling()
    '''
    PROFILE.disable()


''' --------------------- Timers --------------------- '''

class Stopwatch():
    '''tracks time since last clear (via self.reset()), or last "marked" time (via self.mark_time()).
    All times are in seconds.

    _t_0: time at "t=0". Set by self.reset()
    _t_marks: "marked" times (a dict). Default has key=None.
                Set by self.mark_time(). Note: self.reset() also removes ALL marked times.
    _t_create: time when self was created (never changes).
    '''
    def __init__(self):
        self.reset()
        self._t_create = self._t_0

    # time coordinate setters #
    def reset(self, *, _time=None):
        '''sets "t=0" time to now (or _time, if provided). Also removes all "marked" times.'''
        self._t_0 = self.now() if _time is None else _time
        self._t_marks = dict()  # << reset "marked" times.

    def mark_time(self, key=None, *, _time=None):
        '''sets "marked" time to now (or _time, if provided).
        key: hashable object, default None
            associate marked time with this key. (None --> "default" marked time.)
        '''
        self._t_marks[key] = self.now() if _time is None else _time

    # time getters #
    time = property(lambda self: self.time_since_reset, doc='''alias to self.time_since_reset''')
    time_elapsed = time   # << another alias to time_since_reset

    def time_since_reset(self):
        '''returns time [in seconds] since the "t=0" time on this watch.
        "t=0" set by self.reset(). Also set when created.
        '''
        return self.now() - self._t_0

    def time_since_mark(self, key=None):
        '''returns time [in seconds] since the "mark" time on this watch.
        "mark" set by self.mark_time(). Also set when created or reset().

        key: hashable object, default None
            "marked time" associated with this key.
        '''
        return self.now() - self.get_marked_time(key=key)

    def get_marked_time(self, key=None):
        '''returns marked time associated with this key.'''
        return self._t_marks.get(key, self._t_0)

    def get_marked_times(self):
        '''returns dict of marked times.'''
        return self._t_marks

    @staticmethod
    def now():
        '''returns time.time(). Provided as a static method for convenience.'''
        return time.time()

    # display #
    def __repr__(self):
        return f'{type(self).__name__}(time_since_reset = {self.time_since_reset():.2f} seconds)'

    def time_elapsed_string(self, as_string=False):
        '''returns string 'Total time elapsed: {:.2f} seconds.'.format(self.time_since_reset())'''
        return f'Total time elapsed: {self.time_since_reset():.2f} seconds.'


class TickingWatch(Stopwatch):
    '''Stopwatch that also "ticks" every N seconds.
    No active background process; just checks time, when queried. See self.tick().
    
    self.tick() returns whether at least N seconds have passed since the "marked" time,
        AND if the result is True, sets the "marked" time to now.
    self.nticks tracks the number of times self.tick() ever returned True.
        note: self.nticks resets to 0 when self.reset() is called.

    interval: int (possibly negative or 0)
        N. Number of seconds between ticks.
        >0 --> minimum time between calls to self.tick() returning True.
        =0  --> self.tick() will always return True.
        <0 --> self.tick() will always return False.
    wait: bool, default True
        whether the first True tick() may require waiting.
        False --> the first call to self.tick() will always give True.
        True --> the first call to self.tick() will only give True if self.interval seconds have passed.
    _t_mark_key: hashable object, default 'tick'
        key associated with the "marked" time for ticking.
        note: if None, ticks are stored in the "default marked time" key; see Stopwatch for details.
    '''
    def __init__(self, interval, *, wait=True, _t_mark_key='tick'):
        super().__init__()
        self.interval = interval
        self.wait = wait
        self.waiting = wait  # << whether we are waiting for tick.
        self._t_mark_key = _t_mark_key

    def reset(self, **kw):
        '''resets the "t=0" time, also set self.nticks=0.'''
        super().reset(**kw)
        self.nticks = 0

    def time_since_tick(self):
        '''get time since last tick. Note: does not edit the "marked" time.'''
        return self.time_since_mark(key=self._t_mark_key)

    def _mark_tick_time(self, *, _time=None):
        '''mark time of this tick. Intended for internal use, only.'''
        self.mark_time(key=self._t_mark_key, _time=_time)

    def tick(self):
        '''returns True if enough time has passed for the next tick.
        If True, also "marks" the current time, as the previous tick time.
        "Enough time" usually means at least self.interval seconds.
        Exceptions:
            if not self.waiting, return True (and set self.waiting = False)
        '''
        if not self.waiting:
            result = True
            self.waiting = True
        elif self.interval < 0:
            result = False
        else:
            result = (self.time_since_tick() >= self.interval)
        if result:
            self.nticks += 1
            self._mark_tick_time()
        return result

