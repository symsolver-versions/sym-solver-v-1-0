"""
Package Purpose: preset SymbolicObjects for SymSolver

Uses classes defined elsewhere to pre-define some objects for convenience.
E.g. some physical variables, some equations, some math objects.

This file:
Imports the main important objects throughout this subpackage.

Usage notes:
    presets will be stored in PRESETS (a dictionary)
    can also get them via get_presets(requests)
"""

from .presets_tools import (
    PRESETS, get_presets, load_presets,
)

load_presets('REQUIRED', dst=locals())
