"""
File Purpose: binding functions to already-existing classes

Motivation: it is convenient to be able to add many functions to classes after their creation.
This facilitates a useful design principle:
    - when creating a class, only define a minimal set of behaviors.
    - when adding new behaviors to an existing class, bind new attributes/methods to the class,
        OUTSIDE of the class definition, rather than editing the class defintion directly.

    For example, to implement "convert to polynomial in x" functionality, the polynomials subpackage
    in SymSolver will bind new methods like obj.polynomial(x) to Sum and Product, telling them how to
    convert themselves into polynomials. The basic functionality of Sum and Product is untouched;
    but there's an extra method attached to each of them. Additionally, all the polynomial() methods
    are defined directly inside the polynomials subpackage.
    By defining these methods outside the Sum and Product classes, we manage to keep the definitions
    of Sum and Product cleaner (we don't need to add methods for every new behavior directly to those
    classes, which quickly leads to long and less-readable class definitions). We also manage to make
    the definitions more generic - if we don't load the polynomials subpackage, those methods won't
    be attached to Sum and Product, but Sum and Product will otherwise still work just fine.

Usually it is sufficient to use this module in the following way:
    from binding_module import Binding
    binding = Binding(locals())

    class MyClass(...):
        # this class could be defined here, or literally anywhere else.
        # e.g. "from module_with_class import MyClass" is fine too.

    with binding.to(MyClass):
        @binding
        def foo(self, *args, **kw):
            # < code for foo goes here

    print(MyClass.foo)
    --> (info about the bound method foo of MyClass, defined above)
    print(foo)
    --> (NameError; foo is undefined in this namespace after exiting the 'with' block.)
"""

from .oop_misc import (
    alias, alias_to_result_of,
)


def bind_to(*targets):
    '''returns a function decorator which binds f to target.(f.__name__) for target in targets.
    This decorator returns the unbound version of f.
    Example, equivalent to defining foo inside MyClass, and also in the local namespace:
        @bind_to(MyClass)
        def foo(self, args):
            # <code for foo>
    '''
    def bind_then_return_f(f):
        '''binds f to targets, then returns f.'''
        for target in targets:
            setattr(target, f.__name__, f)
        return f
    return bind_then_return_f

def bound_to(target):
    '''returns a function decorator which binds f to target.(f.__name__).
    This decorator returns the bound version of f.
    Example, equivalent to defining foo inside MyClass, and also putting foo = target.foo:
        @bound_to(MyClass)
        def foo(self, args):
            # <code for foo>
    '''
    def bind_then_return_f(f):
        '''binds f to targets, then returns f.'''
        setattr(target, f.__name__, f)
        return getattr(target, f.__name__)
    return bind_then_return_f

class Binder():
    '''helps with binding functions (or classes) to classes, and routines to cleanup the local namespace afterwards.
    If you aren't worried about messing with the local namespace, you can just use tools.bind_to, instead.

    This class addresses the issue that a decorated function will also be defined in the local namespace.
        Example, equivalent to defining foo inside MyClass, and also in the local namespace:
            @tools.bind_to(MyClass)
            def foo(self, args):
                # <code for foo>
    This might not be desireable, for two reasons:
        1) if foo was previously defined in the local namespace, it will be overwritten.
        2) afterwards, foo will be set in the local namespace.
    These issues could be easily addressed for a single function e.g.:
        1) beforehand, store original foo in tmp=foo. After, do foo=tmp.
        2) afterwards, could use del foo to remove foo from local namespace if desired.
    But this single-function solution may become tedious if binding many functions.

    To address these issues, the Binder class:
        1) saves a reference to origin function if it was previously defined in the namespace.
        2) tracks functions which have been bound and should be handled "at cleanup".
        - also, provides a cleanup() method which:
        1) restores original function if it was previously defined
        2) deletes all other functions which should be deleted to address problem 2.

    Example:
        foo2 = old_foo_2

        binder = Binder(locals())   # need to input locals() here, so Binder knows which namespace to clean.

        @binder.bind_to(MyClass)
        def foo1(self, args):
            # <code for foo1>
        
        @binder.bind_to(MyClass)
        def foo2(self, args):
            # <code for foo2>

        binder.cleanup()   # removes foo1 from the locals() namespace; restores foo2 = old_foo_2.

    ARGS:
        namespace: dict
            the namespace to clean up upon exiting the context.
            This class was designed with the intention that:
                namespace=locals().
        keep_local: bool, default False
            default value for whether to keep local copies of the bound functions.
            E.g. if self.keep_local=True, then by default we will keep local copies.
    '''
    def __init__(self, namespace, keep_local=False):
        self.namespace = namespace
        self.keep_local = keep_local
        self._reset_flagged()

    def _reset_flagged(self):
        '''resets/creates flagged trackers in self.'''
        self.flagged_for_restoration = dict()
        self.flagged_for_deletion = []
        self.flagged = []

    def bind_to(self, *targets, keep_local=None, target_attr=None):
        '''returns a function decorator which binds f to target.(f.__name__) for target in targets.

        By default, this decorator returns None, and flags fname (=f.__name__) appropriately for handling at cleanup.
            if fname was previously defined in self.namespace, restore original value during cleanup.
                I.e. during cleanup: self.namespace[fname] = funcname's original value
            else, delete from namespace during cleanup.
                I.e. during cleanup: del self.namespace[fname]

        keep_local: bool or None, default None
            if None, use self.keep_local instead. (self.keep_local=False by default)
            if True, instead don't flag for cleanup, and return the original function.
        target_attr: str or None, default None
            if not None, bind to target.(target_attr) instead of target.(f.__name__).
        '''
        if keep_local is None: keep_local = self.keep_local
        if keep_local:
            def bind_then_return_f(f):
                '''binds f to targets, then return f.'''
                self.direct_bind(f, *targets, keep_local=True, target_attr=target_attr)
                return f
            return bind_then_return_f
        else:
            def bind_then_flag_and_return_none(f):
                '''binds f to targets, flags f.__name__ for deletion from self.namespace, then returns None.'''
                self.direct_bind(f, *targets, keep_local=False, target_attr=target_attr)
                return None
            return bind_then_flag_and_return_none

    def direct_bind(self, f, *targets, keep_local=None, target_attr=None):
        '''binds f to target.(f.__name__) for target in targets, then flags f appropriately for cleanup.
        named "direct" bind because this is not a function decorator.

        keep_local: bool or None, default None
            if None, use self.keep_local instead. (self.keep_local=False by default)
            if True, instead don't flag for cleanup, and return the original function.
        target_attr: str or None, default None
            if not None, bind to target.(target_attr) instead of target.(f.__name__).
        '''
        if keep_local is None: keep_local = self.keep_local
        fname = f.__name__
        aname = fname if target_attr is None else target_attr
        # bind to targets
        for target in targets:
            setattr(target, aname, f)
        # handle flagging appropriately for cleanup later.
        if not keep_local:
            if fname in self.flagged:
                return   # already flagged; skip.
            self.flagged.append(fname)
            if fname in self.namespace:
                self.flagged_for_restoration[fname] = self.namespace[fname]
            else:
                self.flagged_for_deletion.append(fname)

    def cleanup(self):
        '''cleans up self.namespace appropriately, following these rules:
        1) previously-defined objects in self.namespace (defined first without the @bind_to decorator)
            which were overwritten to point at an object defined via @bind_to will be restored to their original values.
        2) previously undefined objects in self.namespace
            which were written to point at an object defined via @bind_to will be removed from self.namespace.
        '''
        for name in self.flagged_for_deletion:
            del self.namespace[name]
        for fname, forig in self.flagged_for_restoration.items():
            self.namespace[fname] = forig
        self._reset_flagged()


class Binding():
    '''context manager for binding environment.
    provides these function decorators:
        @self.bind() (or @self) --> bind function or class to each target from self.default_targets.
        @self.bind_to(*targets) --> bind function or class to each target from targets.
    bindings at the corresponding attribute name e.g. target.(func.__name__) = func.
    Upon exiting, restore original state for names in self.namespace of any objects
        decorated via @self.bind() (or @self) or @self.bind_to().

    To bind, but ALSO keep the function in the local namespace (i.e. don't remove it upon exiting),
        use keep_local=True in the decorator. E.g. @self.bind(keep_local=True)

    EXAMPLE:
    # originally, foo1, Foo3, foo4, foo5 undefined, but foo2 defined as foo2_original.
    with Binding(locals(), MyClass) as binding:
        @binding.bind()
        def foo1(args):          # binds MyClass.foo1 = foo1
            # <code for foo1>

        @binding.bind_to(MySecondClass)
        def foo2(args):          # binds MySecondClass.foo2 = foo2
            # <code for foo2>

        @binding  # alias for @binding.bind()
        class Foo3(args):        # binds MyClass.Foo3 = Foo3
            # <code for Foo3>

        @binding.bind_to(MySecondClass, MyThirdClass)
        def foo4(args):          # binds MySecondClass.foo4 = foo4; MyThirdClass.foo4 = foo4
            # <code for foo4>

        @binding.bind(keep_local=True)
        def foo5(args):          # MyClass.foo5 = foo5
            # <code for foo5> 

    # upon exiting, alter the local namespace: delete foo1, Foo3, foo4; set foo2 = foo2_original.
    # (do not delete foo5 from the namespace since keep_local=True was used for foo5.)

    ARGS:
        namespace: dict
            the namespace to clean up upon exiting the context.
            This class was designed with the intention that:
                namespace=locals().
        *default_targets: classes
            the targets to which objects decorated with @self.bind() will be bound
            (roughly, via Binder.bind_to(*default_targets).)
            If this list is empty, trying to use self.bind() will raise a ValueError.

            To enter custom targets, use self.bind_to instead.
        keep_local: bool, default False
            default value for whether to keep local copies of the bound functions.
            E.g. if self.keep_local=True, then by default we will keep local copies.
    '''
    # # # ESSENTIAL BEHAVIOR # # #
    def __init__(self, namespace, *default_targets, keep_local=False):
        self.namespace = namespace
        self.default_targets = default_targets
        self._keep_local = keep_local
        
    @property
    def keep_local(self):
        '''whether to keep local copies of the bound functions.
        when getting keep_local,
            get self._temporary_keep_local if it exists, otherwise self._keep_local.
            Note: _temporary_keep_local is deleted when exiting context (i.e. the "with self..." block).
        '''
        try:
            return self._temporary_keep_local
        except AttributeError:
            return self._keep_local
    @keep_local.setter
    def keep_local(self, value):
        '''set value of keep_local.'''
        self._keep_local = value
        self._temporary_keep_local = value
        
    def __enter__(self):
        self.binder = Binder(self.namespace)

    def bind(self, keep_local=None, target_attr=None):
        '''returns decorator which binds f to target.(f.__name__) for target in self.default_targets,
        and also flags f appropriately for cleanup later when exiting the Binding context.

        keep_local: bool or None, default None
            if None, use self.keep_local instead. (self.keep_local=False by default)
            if True, instead don't flag for cleanup, and return the original function.
        target_attr: str or None, default None
            if not None, bind to target.(target_attr) instead of target.(f.__name__).
        '''
        if keep_local is None: keep_local = self.keep_local
        if len(self.default_targets) > 0:
            return self.binder.bind_to(*self.default_targets, keep_local=keep_local, target_attr=target_attr)
        else:
            raise ValueError('self.bind() uses self.default_targets, but self.default_targets is empty!')

    @property
    def bind_to(self):
        '''returns decorator_maker(*targets, keep_local=None, target_attr=None), which returns a function decorator
        that binds f to target.(f.__name__) for target in targets, and also flags f appropriately
        for cleanup later when exiting the Binding context.

        keep_local: bool or None, default None
            if None, use self.keep_local instead. (self.keep_local=False by default)
            if True, instead don't flag for cleanup, and return the original function.
        target_attr: str or None, default None
            if not None, bind to target.(target_attr) instead of target.(f.__name__).
        '''
        return self.binder.bind_to

    def __exit__(self, *_unused_args):
        self.binder.cleanup()
        try:
            del self._temporary_keep_local
        except AttributeError:
            pass  # just wanted to delete _temporary_keep_local if it existed, but it doesn't.

    # # # FREQUENTLY USED CONVENIENCE # # #
    def to(self, *targets, keep_local=None):
        '''sets targets in self then returns self.
        convenient for writing context entry like this:
        binding = Binding(locals())
        with binding.to(MyClass1):
            @binding.bind()
            def foo1(args):
                #...

        if keep_local is not None:
            also sets _temporary_keep_local in self,
            which overrides self.keep_local until end of 'with' block.
        '''
        self.default_targets = targets
        if keep_local is not None:
            self._temporary_keep_local = keep_local
        return self

    targetting = alias('to')

    __call__ = alias_to_result_of('bind',
        doc='''alias to result of self.bind().
        this means you can write @self instead of @self.bind(). E.g:
        binding = Binding(locals())
        with binding.to(MyClass1):
            @binding
            def foo1(args):
                #...
        ''')

    # # # LESS-OFTEN USED CONVENIENCE # # #
    def with_targets(self, *targets):
        '''return Binding object with same namespace as self, but using the provided targets instead.'''
        return Binding(self.namespace, *targets)

    with_target = alias('with_targets')

    target = property(lambda self: self.get_target(),
            lambda self, value: self.set_target(value),
            doc='''@self.bind() binds objects to target.(object.__name__).
            When len(self.default_targets)==0, roughly an alias to self.default_targets.
            Otherwise irrelevant; see self.default_targets instead.''')

    def get_target(self):
        '''gets binding target of self. makes ValueError if self has not precisely 1 target.'''
        if len(self.default_targets) == 1:
            return self.default_targets[0]
        else:
            raise ValueError(f'zero or multiple targets detected: {self.default_targets}')

    def set_target(self, target):
        '''sets binding target of self to target. (used in self.bind)'''
        self.default_targets = [target]