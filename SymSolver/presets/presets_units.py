"""
File Purpose: defines "units" preset values
"""

from .presets_tools import set_presets
from ..units import UNI


''' --------------------- Units --------------------- '''

# [TODO] any units we want to load here in particular??
# Note: can already get units from UNI.
#   E.g. UNI.L, UNI.newton. See help(UNI) for more details.


''' --------------------- Set Presets --------------------- '''

PRESETS_MISC = set_presets('UNI', locals(), _kind='UNITS')
