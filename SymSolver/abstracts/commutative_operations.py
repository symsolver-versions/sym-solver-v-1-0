"""
File Purpose: CommutativeObject, CommutativeOperation,
"""
from .abstract_operations import AbstractOperation
from .iterable_symbolic_objects import IterableSymbolicObject
from .simplifiable_objects import (
    SimplifiableObject,
    simplify_op,
)

from .symbolic_objects import (
    is_number, _equals0,
    is_nonsymbolic,
)
from ..tools import (
    apply, dichotomize,
    equals, unordered_list_equals,
)


''' --------------------- CommutativeObject --------------------- '''

class CommutativeObject(IterableSymbolicObject, SimplifiableObject):
    '''Iterable and Simplifiable operation with the commutative property.'''
    def __eq__(self, b):
        try:
            return super(IterableSymbolicObject, self).__eq__(b)  # skip IterableSymbolicObject's __eq__.
        except NotImplementedError:
            return unordered_list_equals(self, b)

    def split(self, func):
        '''splits self by func.
        returns ( self._new(terms such that func(term)), self._new(terms such that func(term)) ).
        if all terms in self have func(term) (or all not func(term)),
            uses self, exactly, instead of self._new, to help indicate nothing was changed.
            (Edge case: if len(self) == 0, return (self, self._new()).)
        '''
        ftrue, ffalse = self.dichotomize(func)
        newtrue  = self
        newfalse = self
        if len(ftrue) == len(self):
            newtrue  = self
            newfalse = self._new(*ffalse)
        else:
            newtrue  = self._new(*ftrue)
            newfalse = self if (len(ffalse) == len(self)) else self._new(*ffalse)
        return (newtrue, newfalse)

    def split_numeric_component(self):
        '''returns (numeric component, non-numeric component) of self.
        if there are no is_number(term) terms in self, non-numeric component will be self, exactly.
        if there are only is_number(term) terms in self, numeric component will be self, exactly.
        '''
        return self.split(is_number)


''' --------------------- CommutativeObject SIMPLIFY_OPS --------------------- '''

@simplify_op(CommutativeObject, alias='_simplify_id')
def _commutative_simplify_id(self, **kw__None):
    '''removes any IDENTITY at top level of self.'''
    try:
        IDENTITY = self.IDENTITY
    except AttributeError:
        return self
    else:
        keep_terms = [term for term in self if not equals(term, self.IDENTITY)]
        if len(keep_terms) == len(self):
            return self  # return self, exactly, to help indicate no changes were made.
        else:
            return self._new(*keep_terms)


''' --------------------- CommutativeOperation --------------------- '''

class CommutativeOperation(CommutativeObject, AbstractOperation):
    '''Iterable and Simplifiable operation with the commutative property.'''
    pass


''' --------------------- CommutativeOperation SIMPLIFY_OPS --------------------- '''

@simplify_op(CommutativeOperation, alias='_evaluate_numbers')
def _commutative_evaluate_numbers(self, **kw):
    '''combines numbers in self by doing self.OPERATION on terms which are numbers.
    Uses Commutative property to provide a result in which the numbers are combined as much as possible.
    '''
    OPERATION = getattr(self, 'OPERATION', None)
    if OPERATION is None:
        return self
    # group terms by symbolic / non-symbolic
    numeric, abstract = dichotomize(self, is_nonsymbolic)
    if len(numeric) < 2:
        return self  # return self, exactly, to help indicate nothing was changed.
    # combine non-symbolic terms
    number = OPERATION(*numeric)
    return self._new(number, *abstract)
