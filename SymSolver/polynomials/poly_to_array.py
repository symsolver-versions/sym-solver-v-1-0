"""
File Purpose: convert Polynomial (or PolyFraction) into numpy array or MPArray

to_array() --> to numpy array of Polynomial (or PolyFraction) objects.
to_mp_array() --> to MPArray storing self.to_array()
to_numpy_array() --> to array of numpy.polynomial.Polynomial objects  (only for SymSolver.Polynomial)

MPArray provides the 'apply' method for applying a function to each object in the array.

[TODO] encapsulate the broadcasting code here, instead of copy-pasting most of it.
"""

from ..tools import ImportFailed
try:
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')

from .polynomial import Polynomial
from .polyfraction import PolyFraction
from ..errors import PolynomialPatternError, PolyFractionPatternError
from ..tools import (
    ProgressUpdatePrinter,
    is_integer,
    itarrayte, MPArray,
    Binding,
)
from ..defaults import DEFAULTS

binding = Binding(locals())


''' --------------------- to_array() --------------------- '''

with binding.to(Polynomial):
    @binding
    def to_array(self, print_freq=None):
        '''converts self into an array of Polynomials.
        if all coefs are scalars, the output will have shape ().
            Note: to get the scalar from an array with shape (), use [()]. E.g. np.array(7)[()] --> 7
        otherwise, the output will have the shape implied by numpy broadcasting rules.
            (The coefficients must be broadcastable with each other)
        '''
        updater = ProgressUpdatePrinter(print_freq, wait=True)
        # bookkeeping related to broadcasting
        updater.print('doing bookkeeping / broadcasting for to_array()')
        keys, coefs  = zip(*self.items())
        broad_arrays = np.broadcast_arrays(*coefs, subok=True)
        coefs_broad  = np.asanyarray(broad_arrays[:])
        broad_shape  = np.broadcast_shapes(*(c.shape for c in coefs_broad))
        if len(broad_arrays) == 0:
            raise PolynomialPatternError('cannot convert Polynomial with no coefficients to array.')
        # get result
        result = np.empty(broad_shape, dtype=object)
        size = result.size
        for ii, (_, midx) in enumerate(itarrayte(broad_arrays[0])):
            updater.print(f'Converting to SymSolver.Polynomial ({ii:5d} of {size:5d})', print_time=True)
            # put result[midx] equal to the (midx)'th polynomial
            slicer = tuple((slice(None), *midx))
            this_poly = self._new( { key: coef for key, coef in zip(keys, coefs_broad[slicer]) } )
            result[midx] = this_poly
        updater.finalize(process_name='Polynomial.to_array()')
        return result

with binding.to(PolyFraction):
    @binding
    def to_array(self, print_freq=None):
        '''converts self into an array of PolyFractions.
        if all coefs are scalars, the output will have shape ().
            Note: to get the scalar from an array with shape (), use [()]. E.g. np.array(7)[()] --> 7
        otherwise, the output will have the shape implied by numpy broadcasting rules.
            (The coefficients must be broadcastable with each other)

        NOTE: not really compatible with masked arrays.
        Instead of using masked arrays, it is recommended to index the input arrays by the mask.
        '''
        updater = ProgressUpdatePrinter(print_freq, wait=True)
        # bookkeeping related to broadcasting
        updater.print('doing bookkeeping / broadcasting for to_array()')
        nkeys, ncoefs = zip(*self.numer.items())
        dkeys, dcoefs = zip(*self.denom.items())
        len_n         = len(ncoefs)
        broad_arrays = np.broadcast_arrays(*ncoefs, *dcoefs, subok=True)
        ncoefs_broad = np.asarray(broad_arrays[:len_n])
        dcoefs_broad = np.asarray(broad_arrays[len_n:])
        broad_shape  = np.broadcast_shapes(*(c.shape for c in ncoefs_broad), *(d.shape for d in dcoefs_broad))
        if len(broad_arrays) == 0:
            raise PolyFractionPatternError('cannot convert PolyFraction to array when len(numer)==len(denom)==0')
        # get result
        result = np.empty(broad_shape, dtype=object)
        size = result.size
        for ii, (_, midx) in enumerate(itarrayte(broad_arrays[0])):
            updater.print(f'Converting to PolyFraction; {ii:5d} of {size:5d}', print_time=True)
            # put result[midx] equal to the (midx)'th polynomial
            slicer = tuple((slice(None), *midx))
            numer = self.numer._new({key: coef for key, coef in zip(nkeys, ncoefs_broad[slicer])})
            denom = self.denom._new({key: coef for key, coef in zip(dkeys, dcoefs_broad[slicer])})
            result[midx] = self._new(numer, denom)
        updater.finalize(process_name='PolyFraction.to_array()')
        return result


''' --------------------- to numpy --------------------- '''

with binding.to(Polynomial):
    @binding
    def to_numpy_array(self, print_freq=None, **kw__np_polynomial):
        '''converts self into an array of numpy polynomials.
        if all coefs are scalars, the output will have shape ().
            Note: to get the scalar from an array with shape (), use [()]. E.g. np.array(7)[()] --> 7
        otherwise, the output will have the shape implied by numpy broadcasting rules.
            (The coefficients must be broadcastable with each other)
        '''
        updater = ProgressUpdatePrinter(print_freq, wait=True)
        # bookkeeping related to broadcasting
        updater.print('doing bookkeeping / broadcasting for to_array()')
        coefs = self.coef_list(reverse=False)
        broad_arrays = np.broadcast_arrays(*coefs, subok=True)
        coefs_broad  = np.asanyarray(broad_arrays[:])
        broad_shape  = np.broadcast_shapes(*(c.shape for c in coefs_broad))
        if len(broad_arrays) == 0:
            raise PolynomialPatternError('cannot convert Polynomial with no coefficients to numpy.')
        # get result
        result = np.empty(broad_shape, dtype=object)
        size = result.size
        for ii, (_, midx) in enumerate(itarrayte(broad_arrays[0])):
            updater.print(f'Converting to numpy.polynomial.Polynomial ({ii:5d} of {size:5d})', print_time=True)
            # put result[midx] equal to the (midx)'th polynomial
            slicer = tuple((slice(None), *midx))
            this_poly = np.polynomial.Polynomial(coefs_broad[slicer], **kw__np_polynomial)
            result[midx] = this_poly
        updater.finalize(process_name='Polynomial.to_numpy()')
        return result

    @binding
    def _to_numpy_single(self, **kw__np_polynomial):
        '''converts self into a single numpy polynomial.
        requires that coefficients in self are 0-d (e.g. not lists, not (N>=1)-dimensional arrays).
        '''
        coefs = self.coef_list(reverse=False, iterable_ok=False)
        result = np.polynomial.Polynomial(coefs, **kw__np_polynomial)
        return result


''' --------------------- to_mp_array() --------------------- '''

with binding.to(Polynomial, PolyFraction):
    @binding
    def to_mp_array(self, print_freq=None, **kw__to_array):
        '''return tools.MPArray(self.to_array()). kwargs go to to_array.'''
        self_as_array = self.to_array(print_freq=print_freq, **kw__to_array)
        return MPArray(self_as_array)