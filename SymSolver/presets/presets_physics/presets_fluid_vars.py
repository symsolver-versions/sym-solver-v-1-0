"""
File Purpose: defines "fluids" preset vars

These are:
    - inherently related to physics; specifically, the fluid equations
    - not required for full functionality in other parts of SymSolver
"""

from ..presets_tools import set_presets
from ...basics import symbols
from ...units import UNI


''' --------------------- Generic Fluid vars --------------------- '''

ns, rhos, Ps, Ts = symbols(('n', r'\rho', 'P', 'T'), ['s'])
us, vs = symbols(('u', 'v'), ['s'], vector=True)
qs, ms = symbols(('q', 'm'), ['s'], constant=True)

ns.units_base = UNI.n
rhos.units_base = UNI.rho
Ps.units_base = UNI.P
Ts.units_base = UNI.K
us.units_base = UNI.u
vs.units_base = UNI.u
qs.units_base = UNI.Q
ms.units_base = UNI.M


''' --------------------- Different subscripts --------------------- '''

n   = ns.del_ss('s')
rho = rhos.del_ss('s')
P   = Ps.del_ss('s')
T   = Ts.del_ss('s')
u   = us.del_ss('s')
v   = vs.del_ss('s')
q   = qs.del_ss('s')
m   = ms.del_ss('s')

ne   = ns.ss('s', 'e')
rhoe = rhos.ss('s', 'e')
Pe   = Ps.ss('s', 'e')
Te   = Ts.ss('s', 'e')
ue   = us.ss('s', 'e')
ve   = vs.ss('s', 'e')
qe   = qs.ss('s', 'e')
me   = ms.ss('s', 'e')

ni   = ns.ss('s', 'i')
rhoi = rhos.ss('s', 'i')
Pi   = Ps.ss('s', 'i')
Ti   = Ts.ss('s', 'i')
ui   = us.ss('s', 'i')
vi   = vs.ss('s', 'i')
qi   = qs.ss('s', 'i')
mi   = ms.ss('s', 'i')


''' --------------------- Misc. vars --------------------- '''

g,       = symbols(('g',), vector=True, constant=True)

g.units_base = UNI.a


''' --------------------- Set Presets --------------------- '''

PRESETS_FLUID_VARS = set_presets(('n' , 'rho' , 'P' , 'T' , 'u' , 'v' , 'q' , 'm' ,
                                  'ns', 'rhos', 'Ps', 'Ts', 'us', 'vs', 'qs', 'ms',
                                  'ne', 'rhoe', 'Pe', 'Te', 'ue', 've', 'qe', 'me',
                                  'ni', 'rhoi', 'Pi', 'Ti', 'ui', 'vi', 'qi', 'mi',
                                  'g', ), locals(), _kind='FLUID_VARS')
