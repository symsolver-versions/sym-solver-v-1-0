"""
File Purpose: "fluids" preset vars & equations

These are:
    - inherently related to physics; specifically, the fluid equations
    - not required for full functionality in other parts of SymSolver

[TODO] more preset fluid equation options
"""

from ..presets_tools import set_presets, load_presets
from ...basics import EquationSystem
from ...defaults import ZERO


''' --------------------- load fluid vars --------------------- '''

fluid_vars = load_presets('FLUID_VARS', dst=locals())
# ^ also loads into this namespace: n, ns, rho, rhos, P, Ps, T, Ts, u, us, v, vs, q, qs, m, ms, g

_em_vars = load_presets('EM_VARS')
E, B, J = (_em_vars[_key] for _key in ('E', 'B', 'J'))


''' --------------------- Fluid equations --------------------- '''

fluid_eqs_dict = {
    'continuity': (n.dpt() + (n * u).div(), ZERO),
    'momentum': (n * u.dt_advective(), -P.grad() / m + n * q / m * (E + u.cross(B))),
}

fluid_eqs_s_dict = {
    'continuity_s': (ns.dpt() + (ns * us).div(), ZERO),
    'momentum_s': (ns * us.dt_advective('s'), -Ps.grad() / ms + ns * qs / ms * (E + us.cross(B))),
}

fluid_eqs = EquationSystem.from_dict(fluid_eqs_dict)
fluid_eqs_s = EquationSystem.from_dict(fluid_eqs_s_dict)

eq_continuity = fluid_eqs['continuity']
eq_continuity_s = fluid_eqs_s['continuity_s']
eq_momentum = fluid_eqs['momentum']
eq_momentum_s = fluid_eqs_s['momentum_s']

eq_continuity_e = eq_continuity_s.ss('s', 'e').with_label('continuity_e')
eq_continuity_i = eq_continuity_s.ss('s', 'i').with_label('continuity_i')
eq_momentum_e = eq_momentum_s.ss('s', 'e').with_label('momentum_e')
eq_momentum_i = eq_momentum_s.ss('s', 'i').with_label('momentum_i')

eq_momentum_e_no_inertia = eq_momentum_e._new(ZERO, eq_momentum_e.rhs, label='eq_momentum_e_no_inertia')


''' --------------------- Set Presets --------------------- '''

PRESETS_FLUIDS = set_presets([*fluid_vars.keys(), 'E', 'B', 'J',
        'fluid_eqs', 'fluid_eqs_s',
        'eq_continuity'  , 'eq_momentum'  ,
        'eq_continuity_s', 'eq_momentum_s',
        'eq_continuity_e', 'eq_momentum_e',
        'eq_continuity_i', 'eq_momentum_i',
        'eq_momentum_e_no_inertia',
        ],
        locals(), _kind='FLUIDS')
