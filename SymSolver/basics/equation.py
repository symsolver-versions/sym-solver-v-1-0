"""
File Purpose: Equation
This file just implements the Equation class, which stores info on lhs and rhs.
For routines that solve equations, see e.g. equation_solving.py.
"""
from .basics_tools import (
    gcf,
)
from .product import Product
from ..abstracts import (
    OperationContainer, BinarySymbolicObject, SimplifiableObject, SubbableObject,
    simplify_op,
    _equals0,
    is_constant,
)
from ..initializers import initializer_for
from ..tools import (
    equals, alias,
)


''' --------------------- Equation math --------------------- '''

def _eqn_math_maker(op, attr_name):
    '''returns a function f(s, b) which performs op;
    if b is an Equation, does op on each side. Otherwise use super().
    '''
    def _eqn_math_doer(self, b):
        '''Does math with equations;
        if b is an Equation:
            returns Equation({op}(self.lhs, x.lhs), {op}(self.rhs, x.rhs))
        else:
            returns {op}(self, x)
        '''
        if isinstance(b, Equation):
            new_lhs = op(self.lhs, b.lhs)
            new_rhs = op(self.rhs, b.rhs)
            return self._new(new_lhs, new_rhs)
        else:
            return getattr(super(Equation, self), attr_name)(b)
    return _eqn_math_doer


''' --------------------- Equation --------------------- '''

class Equation(OperationContainer, BinarySymbolicObject,
               SimplifiableObject, SubbableObject):
    '''Equation, e.g. x==y.'''
    def __init__(self, lhs, rhs, *, label=None, solved=False, **kw):
        super().__init__(lhs, rhs, **kw)
        self.label = label
        self.solved = solved

    def _init_properties(self):
        '''returns dict of kwargs to use when initializing another instance of type(self) to be like self,
        via self._new(*args, **kw). Note these will be overwritten in _new by any **kw entered.
        '''
        kw = super()._init_properties()
        kw['label'] = self.label
        kw['solved'] = self.solved
        return kw

    lhs = property(lambda self: self.t1,
                   lambda self, val: setattr(self, 't1', val),
                   doc='''left-hand-side of Equation.''')
    rhs = property(lambda self: self.t2,
                   lambda self, val: setattr(self, 't2', val),
                   doc='''right-hand-side of Equation.''')

    # # # ARITHMETIC # # #
    # op(self, other Equation) --> op(self.lhs, other.lhs); op(self.rhs, other.rhs)
    # op(self, non-Equation obj) --> op(self.lhs, obj); obj(self.rhs, obj)
    def _eqn_math(self, b, op, attr_name):
        return _eqn_math_maker(op, attr_name)(self, b)

    def __add__(self, b):      return self._eqn_math(b, lambda x, y: x + y, '__add__')
    def __radd__(self, b):     return self._eqn_math(b, lambda x, y: y + x, '__radd__')
    def __mul__(self, b):      return self._eqn_math(b, lambda x, y: x * y, '__mul__')
    def __rmul__(self, b):     return self._eqn_math(b, lambda x, y: y * x, '__rmul__')
    def __truediv__(self, b):  return self._eqn_math(b, lambda x, y: x / y, '__truediv__')
    def __rtruediv__(self, b): return self._eqn_math(b, lambda x, y: y / x, '__rtruediv__')
    def __pow__(self, b):      return self._eqn_math(b, lambda x, y: x ** y, '__pow__')

    # # # CONVENIENCE # # #
    def subtract_rhs(self):
        '''returns self.lhs - self.rhs = 0.'''
        return self._new(self.lhs - self.rhs, 0)

    def subtract_lhs(self):
        '''returns 0 = self.rhs - self.lhs'''
        return self._new(0, self.rhs - self.lhs)

    def with_label(self, label, *, force_new=True):
        '''returns new equation like self but with the label provided.
        force_new: bool
            whether to make a new equation even if label matches self.label.
        '''
        if force_new or (label != self.label):
            return self._new(*self, label=label)
        else:
            return self

    # # # MARKING SOLVED / UNSOLVED # # #
    # (Methods for actually solving equations should be provided elsewhere.
    # The methods here are just to mark whether the equation is "solved" or not.)
    def marked_as_solved(self):
        '''return copy of self with solved set to True.'''
        return self.with_set_attr('solved', True)
   
    def marked_as_unsolved(self):
        '''return copy of self with solved set to False.'''
        return self.with_set_attr('solved', False)

    as_solved = alias('marked_as_solved')
    as_unsolved = alias('marked_as_unsolved')


@initializer_for(Equation)
def equation(lhs, rhs, *, label=None, **kw):
    '''returns Equation representing lhs == rhs.
    This just means return Equation(lhs, rhs, label=label, **kw).
    '''
    return Equation(lhs, rhs, label=label, **kw)


''' --------------------- Equation SIMPLIFY_OPS --------------------- '''

@simplify_op(Equation)
def _divide_common_factor(self, **kw__None):
    '''removes factor common to both sides, e.g.: a b x = a c y --> b x = c y.
    g x = 0 --> x = 0;   0 = g x --> 0 = x  (if g is nonzero constant AND x is not constant)
    Does not alter equations where both sides are the same, e.g. x=x.
    '''
    if equals(self.lhs, self.rhs):
        return self
    factor, lhs_over_f, rhs_over_f = gcf(self.lhs, self.rhs)
    # handle a b x = a c y --> b x = c y.
    if not equals(factor, 1):
        return self._new(lhs_over_f, rhs_over_f)
    # handle 'one side is 0'
    else: # equals(factor, 1):
        # handle gx=0 --> x=0 if g nonzero constant AND x non-constant.
        new_lhs = _divide_common_factor_c0_from_sides(self.lhs, self.rhs)
        if new_lhs is not self.lhs:
            return self._new(new_lhs, self.rhs)
        # handle 0=gx --> 0=x if g nonzero constant AND x non-constant.
        new_rhs = _divide_common_factor_c0_from_sides(self.rhs, self.lhs)
        if new_rhs is not self.rhs:
            return self._new(self.lhs, new_rhs)
        # handle "this simplification not applicable" case
        return self

def _divide_common_factor_c0_from_sides(side0, side1):
    '''helper for _divide_common_factor.
    returns None if [gx=0 --> x=0 with nonzero constant g] simplification is not applicable.
    (assumes side0 would be the Product side, and side1 would be the side equal to 0)
    If it is applicable, returns new side0, new side1 after doing the simplification.
    '''
    if isinstance(side0, Product) and _equals0(side1):
        constants, varys = side0.dichotomize(is_constant)
        if len(constants)>0 and len(varys)>0:
            constant = side0._new(*constants)
            if not _equals0(constant):
                return side0._new(*varys)
    return side0