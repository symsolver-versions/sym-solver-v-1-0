"""
Package Purpose: basic-level classes for SymSolver which are important enough for the basics package.
These provide more basic objects to be manipulated by SymSolver users.
These objects are:
    Inequality, MultiSum
Also might put here the slightly more abstract:
    LinearOperator, LinearOperation
but it might make sense for that to go elsewhere, instead...

This file:
Imports the main important objects throughout this subpackage.
"""