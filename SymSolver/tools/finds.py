"""
File purpose: find first index/indices of element/s in list, via find/multifind

These are in their own file to mitigate cyclic import errors with iterables.py.
"""

from .equality import (
    equals,
)


''' --------------------- Find --------------------- '''

def find(x, element, default=None, equals=equals):
    '''find smallest index i for which equals(x[i], element);
    return None if no such i exists.
    '''
    try:
        return next(i for i, elem in enumerate(x) if equals(elem, element))
    except StopIteration:
        return default

def multifind(x, to_find, default=None, require_exists=False, force_unique=True, equals=equals):
    '''return list of (smallest index i for which equals(x[i], element) for element in to_find).
    x: iterable
        the list in which to search for values in to_find
    to_find: iterable
        the values to look for in to_find.
        result will be the list of indices in x of values from to_find.
    default: any value
        use this value instead of an index for each value from to_find which was not found in x.
    require_exists: bool, default False
        if True, require that all values in to_find exist in x,
            and raise ValueError if any values in to_find don't exist in x.
    force_unique: bool, default True
        if True, force that all indices in result will be unique,
            and raise ValueError if this is not possible.
    '''
    result = []
    xlist = list(x)
    i_to_search = set(range(len(xlist)))
    for element in to_find:
        try:
            i_element = (next(i for i in i_to_search if equals(xlist[i], element)))
        except StopIteration:
            if require_exists:
                errmsg = f'{element} not found in {xlist}'
                if len(i_to_search) < len(x):  # add info about i_to_search to avoid confusing message.
                    errmsg += f', in unused indices ({i_to_search})' 
                raise ValueError(errmsg) from None
            else:
                i_element = default
        else:
            if force_unique:
                i_to_search -= {i_element}
        result.append(i_element)
    return result


''' --------------------- Argmin/max --------------------- '''

def argmin(obj):
    '''index of minimum value in obj'''
    return min(range(len(obj)), key=lambda x: obj[x])

def argmax(obj):
    '''index of maximum value in obj'''
    return max(range(len(obj)), key=lambda x: obj[x])