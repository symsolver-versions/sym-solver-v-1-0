"""
Package Purpose: Miscellaneous quality-of-life functions.
This package is intended to provide functions which:
- are helpful for solving specific, small problems
- could be useful in other projects as well
    i.e., these should not depend on other parts of SymSolver.
    (One exception: they may depend on the default values in defaults.py)

This file:
Imports the main important objects throughout this subpackage.
"""

from .arrays import (
    iter_array, itarrayte,
    array_expand_elements,
    ObjArrayInfo, NumArrayInfo,
    stats, array_info, array_info_str,
    array_max, array_min,
    argmax, argmin,
    slicer_at_ax, slice_at_ax, ax_to_abs_ax,
    array_select_max_imag, array_select_min_imag,
    array_select_max_real, array_select_min_real,
)
from .comparisons import (
    skiperror_min, skiperror_max, min_number, max_number,
    similarity, very_similar, maybe_similar, not_similar,
)
from .dicts import (
    dictlike_in_attr, Dict,
)
from .display import (
    _repr, _str, view,
    ViewRateLimiter,
    short_repr, _str_nonsymbolic,
    lightweight_maybe_viewer, MaybeViewer, maybe_viewer,
    print_clear,
    help_str,
)
from .equality import (
    equals,
    list_equals, dict_equals, unordered_list_equals, equal_sets,
    int_equals,
)
from .finds import (
    find, multifind,
    argmin, argmax,
)
from .imports_failed import (
    ImportFailed,
)
from .imports_reload import (
    enable_reload, reload,
)
from .iterables import (
    argsort, nargsort, argsort_none_as_small, argsort_none_as_large, sort_by_priorities,
    counts, counts_idx, counts_sublist_indices, pop_index_tracker, _list_without_i,
    default_sum,
    dichotomize, categorize, Categorizer, group_by,
    deep_iter, layers, structure_string,
    appended_unique,
)
from .numbers import (
    is_integer, is_number, is_real_number,
    infinity, INF, INFINITY,
    POS_INFINITY, POS_INF, PLUS_INF, PLUS_INFINITY,   # aliases for INF
    NEG_INFINITY, NEG_INF, MINUS_INF, MINUS_INFINITY, # aliases for -INF
)
from .mp_tools import (
    MPArray,
    _mpdebug_sleeper, _AttrCaller,
    mp_misc,  # save a reference to mp_misc to easily access newly defined funcs there.
)
from .oop_tools import (
    bind_to, bound_to,
    Binder, Binding,
    StoredInstances,
    apply,
    alias, alias_to_result_of, alias_in,
    caching_attr_simple_if, caching_with_state,
    caching_attr_with_params_if, caching_attr_with_params_and_state_if,
    maintain_attrs, MaintainingAttrs,
    CustomNewDoesntInit,
    Singleton,
    operator_from_str,
    OpClassMeta, Opname_to_OpClassMeta__Tracker, Opname_to_Classes__Tracker,
)
from .plots import (
    extent, get_symlog_ticks, evenly_spaced_idx,
    colors_from_cmap,
    extended_cmap, cmap_extended,
    with_colorbar_extend, _colorbar_extent, _set_colorbar_extend,
    make_colorbar_axes, make_cax, make_colorbar_axis,
    MaintainAxes,
)
from .progress_updates import (
    ProgressUpdatePrinter,
)
from .pytools import (
    format_docstring,
    value_from_aliases,
    assert_values_provided,
    printsource,
    inputs_as_dict, _inputs_as_dict__maker,
    get_locals_up,
    _identity_function,
    documented_namedtuple,
)
from .sentinels import (
    NO_VALUE, RESULT_MISSING,
)
from .sets import (
    Set,
)
from .timing import (
    Profile,
    PROFILE, profiling, print_profile,
    Stopwatch, TickingWatch,
)