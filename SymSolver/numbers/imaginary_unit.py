"""
File Purpose: the imaginary unit, i == 1j == sqrt(-1).
"""

from .abstract_numbers import AbstractNumber
from ..abstracts import (
    simplify_op,
)
from ..basics import (
    Power,
)
from ..initializers import initializer_for, INITIALIZERS
from ..tools import (
    equals, int_equals,
    is_real_number,
    Singleton,
    Binding,
)
binding = Binding(locals())

from ..defaults import DEFAULTS, ZERO, ONE


''' --------------------- ImaginaryUnit --------------------- '''

class ImaginaryUnit(AbstractNumber, Singleton):
    '''the imaginary unit, i.'''
    def evaluate(self):
        '''returns 1j, because it is the number which self represents.'''
        return 1j

    def _repr_contents(self):
        '''returns an empty tuple; just represent self as ImaginaryUnit().'''
        return ()

    def __str__(self):
        return DEFAULTS.IMAGINARY_UNIT_STR

    def __eq__(self, b):
        return self is b   # << (because ImaginaryUnit is a singleton.)


''' --------------------- ImaginaryUnitPower --------------------- '''

class ImaginaryUnitPower(AbstractNumber):
    '''the imaginary unit raised to a power.
    Can be simplified via the rules implied by i**2 == -1.
    '''
    def __init__(self, exponent):
        self.exponent = exponent

    def evaluate(self):
        '''returns (1j)**self.exponent.'''
        return 1j ** self.exponent

    def _repr_contents(self, **kw):
        '''returns contents to put inside 'ImaginaryUnitPower()' in repr for self.'''
        return [f'exponent={self.exponent}']

    def __str__(self):
        i = DEFAULTS.IMAGINARY_UNIT_STR
        ibase = f'{i}' if len(i)==1 else f'{{{i}}}'
        return f'{ibase}^{{{self.exponent}}}'

    def __eq__(self, b):
        '''returns self == b.
        True means self == b, but False could just mean we don't know the answer.
        Does not consider the identity i**2 == -1; use self.apply('simplify_id') first, instead.
        Also does not consider the identity i**1 == i. use self.apply('simplify_id') first, instead.
        '''
        if isinstance(b, type(self)):
            return equals(self.exponent, b.exponent)
        else:
            return False


''' --------------------- SIMPLIFY for ImaginaryUnitPower --------------------- '''

@simplify_op(ImaginaryUnitPower, alias='_simplify_id')
def _imaginary_unit_power_simplify_id(self, **kw__None):
    '''returns sign * i**N, with N from 0 (inclusive) to 2 (exclusive), and sign may be 1 or -1.
    if is_real_number(N), N must be able to handle operations: N % 4, and maybe N < 2.
    (regular python builtin real numbers work just fine.)
    '''
    exponent = self.exponent
    if is_real_number(exponent):
        # get exp mod 4 & exp mod 2.
        exp_mod4 = exponent % 4   # i**2 == -1 --> i**exp == i**(exp % 4)
        # check simple cases (0,1,2,3) first because they are the most likely:
        if exp_mod4 == ZERO:
            return ONE
        elif exp_mod4 == ONE:
            return ImaginaryUnit()
        elif exp_mod4 == 2:
            return -ONE
        elif exp_mod4 == 3:
            return -ImaginaryUnit()
        # handle all other real number cases.
        elif exp_mod4 < 2:
            if exp_mod4 == exponent:
                return self  # return self, exactly, to help indicate nothing was changed.
            else:
                return self._new(exp_mod4)
        else:
            return -self._new(exp_mod4 - 2)
    return self  # return self, exactly, to help indicate nothing was changed.

@simplify_op(Power)
def _power_upcast_to_imaginary_unit_power(self, **kw__None):
    '''if self.base is an ImaginaryUnit, return ImaginaryUnitPower(self.exp).'''
    if isinstance(self.base, ImaginaryUnit):
        return ImaginaryUnitPower(self.exp)
    else:
        return self  # return self, exactly, to help indicate nothing was changed.


''' --------------------- DISPLAY for ImaginaryUnit & ImaginaryUnitPower --------------------- '''

with binding.to(ImaginaryUnit, ImaginaryUnitPower):
    @binding
    def _str_protect_product_factor(self, **kw__None):
        '''returns False, because str doesn't need protecting if it appears as a factor in Product.'''
        return False