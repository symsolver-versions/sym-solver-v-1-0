"""
File Purpose: EquationSystem
"""
from .equation import Equation
from ..abstracts import (
    OperationContainer, IterableSymbolicObject, SimplifiableObject, SubbableObject,
    SymbolicObject,
)
from ..initializers import initializer_for, INITIALIZERS
from ..tools import (
    equals,
    format_docstring,
)

class EquationSystem(OperationContainer, IterableSymbolicObject,
                     SimplifiableObject, SubbableObject):
    '''system of equations, e.g. {x+y==7, y**2/x+z**4==0, z==3}
    Can have any number of equations, possibly even 0 equations.
    Note: attempts to follow the "immutable" design intention.
        Rather than changing an EquationSystem, make a new one with the desired changes.

    Duing substitution algorithms, can use kwarg unsolved_only=False to also sub into "solved" equations.
    '''
    # # # CREATING / INSTANTIATION # # #
    @classmethod
    def from_dict(cls, label_to_eqn_or_tuple):
        '''create an equation system given a dict which maps from labels to eqns;
        the eqns can be represented as Equation or tuple objects.
        Recommend: use string keys to allow index-by-label option for EquationSystem.

        [TODO] use classmethods instead of just the INITIALIZERS code;
            though the INITIALIZERS code could still point to the appropriate classmethods.
            Note: this method doesn't use cls; it always creates an EquationSystem.
        '''
        d = label_to_eqn_or_tuple
        return INITIALIZERS.equation_system(*d.values(), labels=d.keys())

    # # # COMBINING EQUATION SYSTEMS / SLICING EQUATION SYSTEM # # #
    def __add__(self, x):
        '''return self + x. If x is an EquationSystem, return system joining self and x.
        Otherwise, add x to every side of every equation in self.
        '''
        if isinstance(x, EquationSystem):
            return self.append(x)
        else:
            return super().__add__(x)

    # # # SUBSTITUTIONS # # #
    def _subs_check_term(self, eq, unsolved_only=True, **kw__super):
        '''returns whether to check eq during substitutions for self.
        if unsolved_only, check eqs only if they are not marked as solved.
        otherwise, check all eqs.
        '''
        return (not eq.solved) if unsolved_only else super()._subs_check_term(eq, **kw__super)

    # # # INDEXING # # #
    def _term_index_equality_check(self, item_from_self, term):
        '''tells whether item_from_self equals term for the purposes of indexing (see self.get_index).
        Specifically, also checks here if LHS of equation equals term. This allows indexing by LHS.
        Examples of equivalency of different indexing options:
        eqs = EquationSystem(Equation(x, 7), Equation(y, 1+z), Equation(u**2, -1)),
            eqs[0] <--> eqs[x] <--> eqs[Equation(x, y)];
            eqs[1] <--> eqs[y] <--> eqs[Equation(y, 1+z)];
            eqs[2] <--> eqs[u**2] <--> eqs[Equation(u**2, -1)].
        '''
        return equals(item_from_self[0], term) or super()._term_index_equality_check(item_from_self, term)

    def _is_indexkey(self, key):
        '''returns whether key might be a way to lookup an index of self via self.key_index().
        Here we return isinstance(key, str). I.e. strings (but nothing else) can be indexkeys.
        '''
        return isinstance(key, str)

    def _get_indexkey(self, term):
        '''returns indexkey from term.
        Here we return term.label, I.e. use an equation's "label" attribute its indexkey.
        '''
        return term.label

    @property
    def labels(self):
        '''labels of equations in self.
        Result has same length as self. Label will be None if not found.
        '''
        return tuple(getattr(eq, 'label', None) for eq in self)

    def with_labels(self, new_labels, *, force_new=True):
        '''create new equation system with same equations as in self but using new_labels.
        force_new: bool
            whether to make a new equation even if new label matches equation.label.
        '''
        return self._new(*(eqn.with_label(newl, force_new=force_new) for eqn, newl in zip(self, new_labels)))

    def relabeled_via(self, relabeler, *, force_new=True):
        '''create new equation system with same equations as in self but using relabeler to adjust labels.
        new_label = relabeler(old label).
        force_new: bool
            whether to make a new equation even if new label matches equation.label.
        '''
        return self._new(*(eqn.with_label(relabeler(eqn.label), force_new=force_new) for eqn in self))

    # # # SOLVED / UNSOLVED -- TRACKING # # #
    solved = property(lambda self: tuple(i for i, eq in enumerate(self) if eq.solved),
            doc='''indices corresponding to solved equations in self.''')
    unsolved = property(lambda self: tuple(i for i, eq in enumerate(self) if not eq.solved),
            doc='''indices corresponding to unsolved equations in self.''')

    def marked_as_solved(self, i):
        '''return copy of self with eqn(s) i marked as solved.
        i: slice, int, SymbolicObject, or list of ints or SymbolicObjects.
        '''
        generic_idx = self.generic_indices(i)
        return self._new(*((eq.marked_as_solved() if j in generic_idx else eq) for j, eq in enumerate(self)) )

    def marked_as_unsolved(self, i):
        '''return copy of self with eqn(s) i marked as unsolved.'''
        generic_idx = self.generic_indices(i)
        return self._new(*((eq.marked_as_unsolved() if j in generic_idx else eq) for j, eq in enumerate(self)) )

    def unsolved_system(self):
        '''return EquationSystem with only the unsolved equations from self.'''
        return self._new(*(self[i] for i in self.unsolved))

    # # # SOLVING # # #
    def solution_put(self, i_eqn, var, subs=False, **kw__solve):
        '''solve eqn i_eqn for var var, then return new system where i_eqn is that solution and is marked as solved.
        if subs, also sub the solution into all the other unsolved equations in the result.
        '''
        if subs:
            result = self.solution_put(i_eqn, var, subs=False, **kw__solve)  # first solution_put without subs
            result = result.subs(result[i_eqn], unsolved_only=True, **kw__solve)
            return result
        eqns = list(self)
        eqns[i_eqn] = eqns[i_eqn].solve(var, **kw__solve)
        return self._new(*eqns)

    def put_solved_equation(self, ieq, solved_eq):
        '''return copy of self with self[ieq] replaced by solved_eq.marked_as_solved().'''
        eqsolved = solved_eq.marked_as_solved()
        eqns = list(self)
        eqns[ieq] = eqsolved
        return self._new(*eqns)


@initializer_for(EquationSystem)
def equation_system(*eqn_or_tuple_objects, labels=None):
    '''create a new EquationSystem using the equations provided.
    Always returns an EquationSystem, even if no args are entered.

    *args: each should be either an Equation or a tuple.
        tuples will be converted to Equation objects.
        if any args are not an Equation or tuple, raise TypeError.
            (This helps prevent accidental errors when other iterables are involved.)

    labels: None or list with length == number of equations (or tuples) input here.
        if labels[i] is provided, put label=labels[i] for the i'th input.
            (if i'th input was an Equation, make a new equation if the input label doesn't match.)
        Note: labels[i] = None indicates "no label provided".
        See also: EquationSystem.from_dict(labels_to_eqn_or_tuple),
            which provides similar functionality but allows to input a dict instead.

    implementation detail note:
        by using INITIALIZERS.equation, we ensure that equation_system() will remain appropriate
        even if a later module defines a new function as the initializer for equation.
    '''
    eqns = []
    if labels is None: labels = (None for _ in eqn_or_tuple_objects)
    for arg, label in zip(eqn_or_tuple_objects, labels):
        if isinstance(arg, tuple):
            eqn = INITIALIZERS.equation(*arg, label=label)
        elif isinstance(arg, Equation):
            if label is not None:
                eqn = arg.with_label(label, force_new=False)
            else:
                eqn = arg
        else:
            raise TypeError(f'expect all entries to be tuples or Equation but got {type(arg)}')
        eqns.append(eqn)
    return EquationSystem(*eqns)


@format_docstring(equation_system_docs=equation_system.__doc__)
def eqsys(*eqn_or_tuple_objects, labels=None, **kw):
    '''alias for INITIALIZERS.equation_system(...).
    
    {equation_system_docs}
    '''
    return INITIALIZERS.equation_system(*eqn_or_tuple_objects, labels=labels, **kw)
