"""
Package Purpose: efficient polynomials with arbitrary coefficients.

This file:
Imports the main important objects throughout this subpackage.
"""

from .polynomial import (
    Polynomial, _polynomial_math,
)
from .polyfraction import (
    PolyFraction, _polyfraction_math,
)
from .polynomialize import (
    polynomial, polynomialize, _first_denominator_appearance,
)
from .polyfractionize import (
    polyfraction, polyfractionize,
)
from .roots import (
    roots_max_imag, roots_min_imag,
    roots_max_real, roots_min_real,
)


# more imports: modules which augment other objects
# but don't define any new objects for SymSolver.
# import then forget these modules; no need to keep them in this namespace.
from . import poly_to_array
del poly_to_array
