"""
File Purpose: complexity for abstracts
"""
import builtins  # << for unambiguous sum

from .abstract_operations import AbstractOperation
from .associative_operations import AssociativeObject, AssociativeOperation
from .commutative_operations import CommutativeObject, CommutativeOperation
from .iterable_symbolic_objects import IterableSymbolicObject, BinarySymbolicObject
from .keyed_symbolic_objects import KeyedSymbolicObject
from .operation_containers import OperationContainer
from .simplifiable_objects import SimplifiableObject
from .substitutions import SubbableObject
from .symbolic_objects import SymbolicObject

from ..tools import (
    format_docstring,
    Binding, caching_attr_simple_if,
)

from ..defaults import DEFAULTS, ZERO

binding = Binding(locals())

# convenience
def complexity(x):
    '''returns x.complexity() if available, else 1.'''
    try:
        x_complexity = x.complexity
    except AttributeError:
        return 1
    else:
        return x_complexity()

# default complexity for SymbolicObject
with binding.to(SymbolicObject):
    @binding
    def complexity(self):
        '''returns 1, the default complexity for a SymbolicObject.'''
        return 1

# default complexity: 1 + super().complexity()
default_complexity_classes = (
    AbstractOperation,
    AssociativeObject, AssociativeOperation,
    CommutativeObject, CommutativeOperation,
    BinarySymbolicObject,
    KeyedSymbolicObject,
    OperationContainer,
    SimplifiableObject,
    SubbableObject,
)

def _default_complexity_definer(cls):
    '''returns complexity function for cls.'''
    @caching_attr_simple_if(lambda: DEFAULTS.CACHING_PROPERTIES)
    @format_docstring(cls=cls.__name__)
    def complexity(self):
        '''returns 1 + super({cls}, self).complexity().'''
        return 1 + super(cls, self).complexity()
    return complexity

for cls in default_complexity_classes:
    binding.bind_to(cls)(_default_complexity_definer(cls))

# non-default complexities:
with binding.to(IterableSymbolicObject):
    @binding
    @caching_attr_simple_if(lambda: DEFAULTS.CACHING_PROPERTIES)
    def complexity(self):
        '''return sum of complexities of terms in self.'''
        return builtins.sum(complexity(term) for term in self)
