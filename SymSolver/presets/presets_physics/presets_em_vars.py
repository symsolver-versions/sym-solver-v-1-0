"""
File Purpose: defines "E & M" preset vars

These are:
    - inherently related to physics; specifically, the E & M equations
    - not required for full functionality in other parts of SymSolver
"""

from ..presets_tools import set_presets
from ...basics import symbols
from ...units import UNI


''' --------------------- EM vars --------------------- '''

E, B, J = symbols(('E', 'B', 'J'), vector=True)
E.units_base = UNI.E
B.units_base = UNI.B
J.units_base = UNI.J

''' --------------------- Set Presets --------------------- '''

PRESETS_EM_VARS = set_presets('E B J', locals(), _kind='EM_VARS')
