"""
File Purpose: INITIALIZERS

INITIALIZERS provides the functions used to initialize different operations,
when sometimes initializing the operation will actually lead to a different result.
    For example, product(*args) returns a Product object when there are 2 or more args,
    but product(arg) returns arg, while product() returns 1.

These functions are not defined inside this file in order to avoid cyclic dependency.
    For example, product.py defines product() but also needs to know the other initializers,
    such as sum. E.g. Product(x,y)+z --> sum(Product(x,y),z).
Instead, other modules should import this one and put their initializers in the INITIALIZERS object.
"""
import inspect

class _Initializers():
    '''holds the functions for initializing different objects throughout SymSolver.
    For example, sum and product.
    
    Those functions should be attached to the class instance INITIALIZERS after it is created.
    They can be accessed via indexing or as attributes, e.g. INITIALIZERS['sum'] or INITIALIZERS.sum.
    '''
    def __init__(self):
        self._initializers = {}

    @property
    def initializers(self):
        '''{name: initializer} for initializers stored here.
        Note: can also do getattr(self, name), e.g. getattr(self, 'product') or self.product.
        '''
        return self._initializers

    @property
    def modules(self):
        '''{initializer name: module where initializer was defined} for initializers stored here.'''
        return {name: inspect.getmodule(initializer) for name, initializer in self.initializers.items()}

    def __setitem__(self, name, initializer):
        '''sets name in self to initializer.'''
        self.initializers[name] = initializer
        setattr(self, name, initializer)

    def __getitem__(self, name):
        return self.initializers[name]

    def keys(self):
        return self.initializers.keys()

    def append(self, initializer, name=None):
        '''puts initializer into list of initializers.
        name: None or string
            name of attribute of self which will be assigned to initializer.
            e.g. 'product' --> self.product=initializer.
            if None, use initializer.__name__.
        '''
        name = name if name is not None else initializer.__name__
        self[name] = initializer

    def __repr__(self):
        return f'_Initializers object containing ({self.initializers.keys()})'

INITIALIZERS = _Initializers()

def initializer(f):
    '''decorator which returns f but first puts f in list of initializers.'''
    INITIALIZERS[f.__name__] = f
    return f

def initializer_for(target):
    '''factory which returns decorator which returns f but first sets target.initializer.
    uses target.initializer = property(lambda self: f, doc="function used to create a new instance of self")'''
    def _initializer_for_target(f):
        target.initializer = property(lambda self: f, doc='''function used to create a new instance of self''')
        return initializer(f)
    return _initializer_for_target